<h1 align="center">McGuild (by the Div Devs)</h1>

<p align="center">
  <img src="https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white" alt="vite.js">
  <img src="https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB" alt="react">
  <img src="https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white" alt="typescript">
  <img src="https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white" alt="node.js">
  <img src="https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB" alt="express.js">
  <img src="https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white" alt="mongodb">
</p>

## Table of Contents

- [Table of Contents](#table-of-contents)
- [💡 Authors](#-authors)
- [Production version](#production-version)
- [Prerequisites](#prerequisites)
- [🛠️ Installation](#️-installation)
- [💻 Usage](#-usage)

## 💡 Authors

- Constantin Buruiana (<constantin.buruiana@mail.mcgill.ca>) - team leader
  - Responsible for:
    - Project setup
    - Authentication backend
    - Pinning/searching messages backend
- David Castelli (<david.castelli@mail.mcgill.ca>)
  - Responsible for:
    - Boards backend
    - Channel backend
    - Chat backend
- Jackson Evans (<jackson.evans@mail.mcgill.ca>)
  - Responsible for:
    - Login frontend
    - Website styling
- Felis Sedanoluo (<felis.sedanoluo@mail.mcgill.ca>)
  - Responsible for:
    - Board select frontend
    - Board frontend
    - Chat frontend (including DMs)

## Production version

It's deployed on McGill SOCS (use McGill VPN or WiFi to access): <https://fall2023-comp307-group12.cs.mcgill.ca/>

## Prerequisites

Here's the list of utilities you need to install for the project:

- Tested OS's: MacOS, Linux. The project should work on WSL2 and Windows directly as well, let me know if it doesn't.
- Node.js (16+, preferably 20). I suggest using NVM (<https://github.com/nvm-sh/nvm>) or another version manager, but it's not necessary.
- Yarn (1.22+) or Bun (latest) installed globally.
- Docker (20.10+, ideally latest).

## 🛠️ Installation

- Go to the **root directory** of the project:

  ```shell
  cd div-devs-slack
  ```

- Install project dependencies:

  ```shell
  npm run install:all
  ```

## 💻 Usage

- At the root of the project, run the command:

```shell
npm run dev
```

> **Warning**
>
> If you only want to use the client part:
>
> ```shell
> # With NPM:
> npm run client
> # With Yarn:
> yarn run client
> # With Bun:
> bun run client
> ```
>
> Same for the server:
>
> ```shell
> # With NPM:
> npm run server
> # With Yarn:
> yarn run server
> # With Bun:
> bun run server
> ```
>
> Just database:
>
> ```shell
> docker compose up -d db
> ```
