Website deployed at https://fall2023-comp307-group12.cs.mcgill.ca/

Authors:
- Constantin Buruiana (<constantin.buruiana@mail.mcgill.ca>) - team leader
  - Responsible for:
    - Project setup
    - Authentication backend
    - Pinning/searching messages backend
- David Castelli (<david.castelli@mail.mcgill.ca>)
  - Responsible for:
    - Boards backend
    - Channel backend
    - Chat backend
- Jackson Evans (<jackson.evans@mail.mcgill.ca>)
  - Responsible for:
    - Login frontend
    - Website styling
- Felis Sedanoluo (<felis.sedanoluo@mail.mcgill.ca>)
  - Responsible for:
    - Board select frontend
    - Board frontend
    - Chat frontend (including DMs)