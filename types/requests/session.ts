import { Request } from "express";
import { Session, SessionData } from "express-session";
import { IUser } from "@/types/user";

export interface ISessionData extends SessionData {
  user: IUser;
}

export interface IRequest extends Request {
  session: Session & ISessionData;
}
