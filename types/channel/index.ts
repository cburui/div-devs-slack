import ObjectID from "bson-objectid";
import { IUser } from "../user";
import { IMessageWithObjects } from "../message/message";

export interface IChannel {
  _id: ObjectID;
  name: string;
  users: ObjectID[];
  is_DM: Boolean;
  createdAt: Date;
  pinnedMessages: ObjectID[];
}

export interface IChannelWithObject {
  _id: ObjectID;
  name: string;
  users: IUser[];
  is_DM: Boolean;
  createdAt: Date;
  pinnedMessages: IMessageWithObjects[];
}
