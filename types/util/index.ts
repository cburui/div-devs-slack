export type Modify<T, R> = Omit<T, keyof R> & R;

export type SuccessResponse<T> = {
  success: boolean;
  message: string;
  data?: T | null;
};
