import ObjectID from "bson-objectid";
import { IUser } from "../user";

export interface IMessage {
  _id: ObjectID;
  user: ObjectID;
  content: string;
  createdAt: Date;
  channelId: ObjectID;
}

export interface IMessageWithObjects {
  _id: ObjectID;
  user: IUser;
  content: string;
  createdAt: Date;
  channelId: ObjectID;
}
