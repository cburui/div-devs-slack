import ObjectID from "bson-objectid";
import { IUser } from "@/types/user";
import { IChannelWithObject } from "@/types/channel";

export interface IBoard {
  _id: ObjectID;
  name: String;
  admin: ObjectID;
  users: ObjectID[];
  channels: ObjectID[];
}

export interface IBoardWithObjects {
  _id: ObjectID;
  name: String;
  admin: IUser;
  users: IUser[];
  channels: IChannelWithObject[];
}

export interface IBoardResponse {
  boards: IBoardWithObjects[];
}
