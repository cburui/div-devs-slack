import ObjectID from "bson-objectid";
import { IBoard } from "@/types/board";

export enum Role {
  STUDENT = "STUDENT",
  TA = "TA",
  PROFESSOR = "PROFESSOR",
  ADMIN = "ADMIN",
}

export interface IInvite {
  sender: ObjectID;
  board: ObjectID;
}

export interface IInviteWithObjects {
  sender: IPublicUser;
  board: IBoard;
}

export interface IPublicUser {
  _id: ObjectID;
  email: string;
  username: string;
  createdAt: Date;
  avatar?: string;
  roles?: Role[];
  invites?: IInvite[];
}

export interface IUser extends IPublicUser {
  password: string;
}

export interface ILoginResponse {
  user: IPublicUser;
}

export interface ILoginPayload {
  email: string;
  password: string;
}

export interface IRegisterPayload {
  username: string;
  email: string;
  password: string;
}
