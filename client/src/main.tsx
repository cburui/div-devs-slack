import React from "react";
import ReactDOM from "react-dom/client";
import "./main.css";
import { Routes } from "@generouted/react-router";
import UserProvider from "./utils/context/user";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <UserProvider>
      <Routes />
    </UserProvider>
  </React.StrictMode>,
);
