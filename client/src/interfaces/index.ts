import { IChannelWithObject } from "@/types/channel";
import { IMessageWithObjects } from "@/types/message/message";
import { IUser } from "@/types/user";
import ObjectID from "bson-objectid";

export enum ManagerPageType {
  None = "None",
  MangeChannels = "MnanageChannel",
  ManageBoardMembers = "ManageBoardMembers",
  ManageChannelMembers = "ManageChannelMembers",
  ShowPinMessage = "ShowPinMessage",
  RenameChannel = "RenameChannel",
  SearchText = "SearchText",
}

export interface IBoardMetaInfo {
  boardId: ObjectID;
  boardName: string;
  channels: IChannelWithObject[];
  members: IUser[];
  userIsAdmin: boolean;
}

export interface IBoardSideBarProp {
  managerPageType: ManagerPageType;
  boardMetaInfo: IBoardMetaInfo;
  currentChannel: IChannelWithObject | undefined;
  setCurrentChannel: React.Dispatch<
    React.SetStateAction<IChannelWithObject | undefined>
  >;
  // currentChannelId: ObjectID | undefined,
  // setCurrentChannelId: React.Dispatch<React.SetStateAction<ObjectID | undefined>>,
  handleRefreshSidebar: () => Promise<void>;
  handleRefreshChatBox: () => Promise<void>;
  setManagerPageType: React.Dispatch<React.SetStateAction<ManagerPageType>>;
  handleSwitchChannel: (
    channelId: ObjectID,
    chnnel?: IChannelWithObject,
  ) => void;
  handleRemoveBoardMember: (memberId: ObjectID) => Promise<void>;
  handleRemoveChannel: (channelId: ObjectID) => Promise<void>;
  handleStartPrivateMessage: (userToMessage: IUser) => Promise<void>;
}

export interface IBoardChatBoxProp {
  messages: IMessageWithObjects[];
  activeChannel: string;
  userMessage: React.MutableRefObject<string>;
  handleInputChange: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void;
  handleSendMessage: (event: React.FormEvent<HTMLFormElement>) => Promise<void>;
  handleRemoveMessage: (messageId: ObjectID) => Promise<void>;
  handlePinMessage: (messageId: ObjectID) => Promise<void>;
  setManagerPageType: React.Dispatch<React.SetStateAction<ManagerPageType>>;
  boardMetaInfo: IBoardMetaInfo;
}

export interface IBoardManagerPopupProp {
  managerPageType: ManagerPageType;
  setManagerPageType: React.Dispatch<React.SetStateAction<ManagerPageType>>;
  channelToMange?: IChannelWithObject;
  hasMessageFound: boolean;
  messagesFound: IMessageWithObjects[];
  handleInputChange: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void;
  refreshChannel: () => Promise<void>;
  handleAddOperation: (objectToManage: string) => void | Promise<void>;
  handleRemoveOperation: (
    objectToManage: ObjectID,
  ) => void | Promise<void>;
  handleRenameOperation: (
    channelId: ObjectID,
    newChannelName: string,
  ) => void | Promise<void>;
  handleAddMemberChannelOperation: (
    channelId: ObjectID,
    userEmail: string,
  ) => void | Promise<void>;
  handleRemoveMemberChannelOperation: (
    channelId: ObjectID,
    memberId: ObjectID,
  ) => void | Promise<void>;
  handleSearchText: (
    channelId: ObjectID,
    page: number,
    query: string,
  ) => void | Promise<void>;
  handleUnpinMessage: (messageId: ObjectID) => Promise<void>;
  // boardMetaInfo: IBoardMetaInfo,
  // onHandleAddMember
  // onHandleRemoveMember
  // onHandleCreateChannel
  // onHandleDeleteChannel
}

export interface ISearchMessageProp {
  currentChannel?: IChannelWithObject;
  messageQueryRef: React.MutableRefObject<string>;
  hasMessageFound: boolean;
  messagesFound: IMessageWithObjects[];
  managerPageType: ManagerPageType;
  setManagerPageType: React.Dispatch<React.SetStateAction<ManagerPageType>>;
  handleSearchText: (
    channelId: ObjectID,
    page: number,
    query: string,
  ) => void | Promise<void>;
}

export interface IPrivateChatProp {
  userToMessage: IUser;
  boardID: ObjectID;
  setShowPrivateMessage: React.Dispatch<React.SetStateAction<boolean>>;
}
