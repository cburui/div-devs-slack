import { useNavigate } from "@/router";

const Boards: React.FC = () => {
  const navigate = useNavigate();

  return (
    <>
      <div onLoad={() => navigate("/boards/userboards")}></div>
    </>
  );
};

export default Boards;
