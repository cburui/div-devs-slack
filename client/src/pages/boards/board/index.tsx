import Board from "@/components/Board";
import MenuBar from "@/components/MenuBar";
import "./index.css";
import { useLocation } from "react-router-dom";
import { IBoardWithObjects } from "@/types/board";

// export default Board

const BoardPage: React.FC = () => {
  const location = useLocation();
  const board = location.state.board as IBoardWithObjects;
  console.log("navifagted to board with object" + board);

  return (
    <>
      <div className="container__boardcontent">
        <MenuBar />
        <Board {...board} />
      </div>
    </>
  );
};

export default BoardPage;
