import UserBoards from "@/components/UserBoards";
import MenuBar from "@/components/MenuBar";
import "./index.css";
// export default UserBoards;

const UserBoardsPage: React.FC = () => {
  return (
    <>
      <div className="container__userboardscontent">
        <MenuBar />
        <UserBoards />
      </div>
    </>
  );
};

export default UserBoardsPage;
