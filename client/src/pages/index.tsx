import "./index.css";
import { useBoxAnimation } from "@/utils/boxAnimation";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const App = () => {
  const navigate = useNavigate();
  const { clearImages } = useBoxAnimation();
  useEffect(() => {
    return clearImages;
  }, []);

  return (
    <>
      <img src="/McGuild_Sword.png" alt="McGuild Logo" />

      <div className="buttonBox">
        <button className="button login" onClick={() => navigate("/login")}>
          log in
        </button>
        <button
          className="button registration"
          onClick={() => navigate("/register")}
        >
          register
        </button>
      </div>
    </>
  );
};

export default App;
