import React, { useEffect } from "react";
import "./login.css";
import { useBoxAnimation } from "@/utils/boxAnimation";
import { useUserContext } from "@/utils/context/user";
import * as Nope from "nope-validator";
import { nopeResolver } from "@hookform/resolvers/nope";
import { SubmitHandler, useForm } from "react-hook-form";
import { useNavigate } from "@/router";
import { ILoginPayload } from "@/types/user";

const schema = Nope.object().shape({
  email: Nope.string()
    .email("Please use your McGill email for login")
    .required("Please enter your email"),
  password: Nope.string().required("Please enter your password"),
});

const LoginPage: React.FC = () => {
  const {
    register: registerForm,
    formState: { errors },
    handleSubmit,
  } = useForm<ILoginPayload>({
    resolver: nopeResolver(schema),
  });
  const navigate = useNavigate();
  const { clearImages } = useBoxAnimation();
  const {
    actions: { login },
  } = useUserContext();
  useEffect(() => {
    return clearImages;
  }, []);

  const handleLogin: SubmitHandler<ILoginPayload> = async (data) => {
    try {
      await login(data, navigate);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <img src="/McGuild_Sword.png" alt="McGuild Logo" />
      <form onSubmit={handleSubmit(handleLogin)}>
        <div className="formBox">
          <div className="formField">
            <label htmlFor="email">McGill Email:</label>
            <input {...registerForm("email")} />
          </div>

          <div className="formField">
            <label htmlFor="password">Password:</label>
            <input type="password" {...registerForm("password")} />
          </div>
        </div>

        <div className="buttonBox">
          <button type="submit" className="button login">
            log in
          </button>
          <button className="button cancel" onClick={() => navigate("/")}>
            cancel
          </button>
        </div>

        <div className="formBox">
          {errors.email && <p className="error">{errors.email.message}</p>}
          {errors.password && (
            <p className="error">{errors.password.message}</p>
          )}
        </div>
      </form>
    </>
  );
};

export default LoginPage;
