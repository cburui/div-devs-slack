import { useBoxAnimation } from "@/utils/boxAnimation";
import "./register.css";
import { useEffect } from "react";
import { useNavigate } from "@/router";
import { useUserContext } from "@/utils/context/user";
import { SubmitHandler, useForm } from "react-hook-form";
import * as Nope from "nope-validator";
import { nopeResolver } from "@hookform/resolvers/nope";
import { IRegisterPayload } from "@/types/user";

const schema = Nope.object().shape({
  username: Nope.string().required("Please enter a username"),
  email: Nope.string()
    .email("Please use a McGill email for registration")
    .required("Please enter a McGill email"),
  password: Nope.string().required("Please enter a password"),
});

const Register: React.FC = () => {
  const {
    actions: { register },
  } = useUserContext();
  const {
    register: registerForm,
    formState: { errors },
    handleSubmit,
  } = useForm<IRegisterPayload>({
    resolver: nopeResolver(schema),
  });
  const navigate = useNavigate();
  const { clearImages } = useBoxAnimation();
  useEffect(() => {
    return clearImages;
  }, []);

  const handleRegister: SubmitHandler<IRegisterPayload> = async (data) => {
    try {
      await register(data);
      navigate("/login");
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <img src="/McGuild_Sword.png" alt="McGuild Logo" />

      <form className="formBox" onSubmit={handleSubmit(handleRegister)}>
        <div className="formField">
          <label>Nickname:</label>
          <input {...registerForm("username")} />
        </div>

        <div className="formField">
          <label>Your McGill Email:</label>
          <input {...registerForm("email")} />
        </div>

        <div className="formField">
          <label>Password:</label>
          <input type="password" {...registerForm("password")} />
        </div>

        <div className="buttonBox">
          <button type="submit" className="button login">
            register
          </button>
          <button
            type="submit"
            className="button cancel"
            onClick={() => navigate("/")}
          >
            cancel
          </button>
        </div>

        <div className="formBox">
          {errors.username && (
            <p className="error">{errors.username.message}</p>
          )}
          {errors.email && <p className="error">{errors.email.message}</p>}
          {errors.password && (
            <p className="error">{errors.password.message}</p>
          )}
        </div>
      </form>
    </>
  );
};

export default Register;
