// The page displayed when user selected a board (basically top root node for the chat thing)
// the { name: string } is basically an anonymous interface since you need an interface for anything that you want to pass as prop
// it's just a place holder interface, should change it as needed
// BoardSide bar for displaying members, channels and managing them. BoardChatBox for read send messages.
import { useRef, useState } from "react";
import BoardChatBox from "../BoardChatBox";
import BoardSideBar from "../BoardSideBar";
import "./Board.css";
import {
  IBoardChatBoxProp,
  IBoardManagerPopupProp,
  IBoardMetaInfo,
  IBoardSideBarProp,
  ManagerPageType,
} from "@/interfaces";
import BoardManagerPopup from "../BoardManagerPopup";
import { IBoardWithObjects } from "@/types/board";
import { API } from "@/utils/requests/API";
import { IChannel, IChannelWithObject } from "@/types/channel";
import { IMessageWithObjects } from "@/types/message/message";
import { IUser } from "@/types/user";
import DMChatBox from "../DMChatBox";
import ObjectID from "bson-objectid";

const Board: React.FC<IBoardWithObjects> = (boardProp: IBoardWithObjects) => {
  // probably need to create some interface for storing: board meta data, channel list, memberlist, initial channel chat messages.

  const [boardMetaInfo, setBoardMetaInfo] = useState<IBoardMetaInfo>({
    boardId: boardProp._id,
    boardName: boardProp.name as string,
    channels: boardProp.channels,
    members: boardProp.users,
    userIsAdmin: true,
  });
  const [managerPageType, setManagerPageType] = useState<ManagerPageType>(
    ManagerPageType.None,
  ); //The type of operation to do for the ManagerPopup
  const [currentChannelId, setCurrentChannelId] = useState<ObjectID>(new ObjectID()); //current active channel on the message display area
  const [currentChannelName, setCurrentChannelName] = useState<string>(""); //current active channel on the message display area
  const [currentChannel, setCurrentChannel] = useState<IChannelWithObject>();
  const [allMessages, setAllMessages] = useState<IMessageWithObjects[]>([]); // all messages to be displayed at message display area
  const messageTextRef = useRef<string>(""); // the message current user will send

  const [messagesPage, _setMessagesPage] = useState<number>(1);
  const [searchPage, _setSearchPage] = useState<number>(1);
  const [hasMessageFound, setHasMessageFound] = useState(false);
  const [messagesFound, setMessagesFound] = useState<IMessageWithObjects[]>([]);

  const [showPrivateMessage, setShowPrivateMessage] = useState(false);
  const [userSelected, setUserSelected] = useState<IUser | undefined>();

  const refreshSideBar = async () => {
    try {
      const obj = await API.get<IBoardWithObjects>(
        `/board-select/board/${boardProp._id}`,
      );
      console.log(obj.success);
      console.log(obj);
      if (obj.success) {
        const boardObject: IBoardWithObjects = obj.data as IBoardWithObjects;
        console.log("Successfully refreshed sidebar " + boardObject);
        // console.log("channels are " + boardObject.channels[0].pinnedMessages[0].content);
        setBoardMetaInfo({
          boardId: boardObject._id,
          boardName: boardObject.name as string,
          channels: boardObject.channels,
          members: boardObject.users,
          userIsAdmin: true,
        });
      }
    } catch (error) {
      console.error("refresh sidebar error: ", error);
    }
  };

  const refreshChannel = async (
    channelId: ObjectID | undefined = currentChannelId,
  ) => {
    if (!channelId) {
      return;
    }
    try {
      const obj = await API.get<IChannelWithObject>(
        `/board-select/board/${boardProp._id}/channel/${channelId.toString()}`,
      );
      console.log(obj.success);
      console.log(obj);
      if (obj.success) {
        const channelObject: IChannelWithObject =
          obj.data as IChannelWithObject;
        console.log("Successfully refreshed sidebar");
        console.log(channelObject);
        setCurrentChannel(channelObject);
      }
    } catch (error) {
      console.error("refresh sidebar error: ", error);
    }
  };

  const refreshChatBox = async (
    channelId: ObjectID = currentChannelId,
    page: number = messagesPage,
  ) => {
    try {
      const messages = await API.get<IMessageWithObjects[]>(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${channelId.toString()}/message?page=${page}`,
      );
      console.log(messages.data);
      if (messages.data) {
        setAllMessages(messages.data);
        document.getElementById("endofmessage")?.scrollIntoView();
      } else {
        setAllMessages([]);
      }
    } catch (error) {
      console.log("refresh chatbox error " + error);
    }
  };

  const handleSearchMessage = async (
    channelId: ObjectID = currentChannelId,
    page: number = searchPage,
    query: string,
  ) => {
    // logic for api call to backend
    try {
      const messages = await API.get<IMessageWithObjects[]>(
        `/board-select/board/${boardMetaInfo.boardId}/channel/${channelId}/message/search?searchText=${query}&page=${page}`,
      );
      console.log(messages.data);
      if (messages.data) {
        setMessagesFound(messages.data);
        setHasMessageFound(messages.data.length > 0);
      } else {
        setHasMessageFound(false);
      }
    } catch (error) {
      console.log("search text error " + error);
    }
  };

  const handleAddMemberBoard = async (userEmail: string) => {
    try {
      await API.put(`/board-select/board/${boardMetaInfo.boardId}/member`, {
        email: userEmail,
      });
      await refreshSideBar();
    } catch (error) {
      console.log("add member to board error " + error);
    }
  };

  const handleRemoveMemberBoard = async (memberId: ObjectID) => {
    try {
      await API.del(
        `/board-select/board/${boardMetaInfo.boardId}/member/${memberId}`,
      );
      await refreshSideBar();
    } catch (error) {
      console.log("remove member from board error " + error);
    }
  };

  const handleCreateChannel = async (channelName: string) => {
    try {
      const channelInfo = await API.post(
        `/board-select/board/${boardMetaInfo.boardId}/channel`,
        { name: channelName },
      );
      console.log("create board successful");
      console.log(channelInfo.data);
      // setCurrentChannelId(channelName);
      await refreshSideBar();
    } catch (error) {
      console.log("create channel error " + error);
    }
  };

  const handleRemoveChannel = async (channelId: ObjectID) => {
    try {
      await API.del(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${channelId.toString()}`,
      );
      await refreshSideBar();
    } catch (error) {
      console.log("remove channel error " + error);
    }
  };

  const handleRenameChannel = async (
    channelId: ObjectID,
    newChannelName: string,
  ) => {
    try {
      await API.put(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${channelId.toString()}`,
        { name: newChannelName },
      );
      await refreshSideBar();
    } catch (error) {
      console.log("rename channel error " + error);
    }
  };

  const handleAddMemberChannel = async (
    channelId: ObjectID,
    userEmail: String,
  ) => {
    try {
      const obj = await API.put(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${channelId.toString()}/member`,
        { email: userEmail },
      );
      console.log("addmember " + obj.message + obj.success + obj.data);
      await refreshSideBar();
    } catch (error) {
      console.log("add member to channel error " + error);
    }
  };

  const handleRemoveMemberChannel = async (
    channelId: ObjectID,
    memberId: ObjectID,
  ) => {
    try {
      const obj = await API.del(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${channelId.toString()}/member/${memberId.toString()}`,
      );
      console.log(
        "inside remove member " + obj.message + obj.success + obj.data,
      );
    } catch (error) {
      console.log("remove member from channel error " + error);
    }
  };

  const handleSwitchChannel = async (
    channelId: ObjectID,
    channel?: IChannelWithObject,
  ) => {
    // event.preventDefault();
    if (channel) {
      const channelInfo = await API.get<IChannelWithObject>(
        `/board-select/board/${
          boardProp._id
        }/channel/${channel._id.toString()}`,
      );
      console.log(channelInfo.data);
      setCurrentChannel(channelInfo.data!);
      setCurrentChannelName(channelInfo.data!.name);
      setCurrentChannelId(channelInfo.data!._id);
      await refreshChannel(channelInfo.data!._id);
      await refreshChatBox(channelInfo.data!._id, 1);
    } else {
      const channelInfo = await API.get<IChannel>(
        `/board-select/board/${
          boardProp._id
        }/channel/${channelId.toString()}/message`,
      );
      console.log(channelInfo.data);
      setCurrentChannelName(channelInfo.data!.name);
      setCurrentChannelId(channelInfo.data!._id);
      await refreshChannel(channelInfo.data!._id);
      await refreshChatBox(channelInfo.data!._id, 1);
    }
  };

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    const { value } = event.target;
    messageTextRef.current = value;
  };

  const handleSendMessage = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (messageTextRef.current != "") {
      // Logic for sending message api call
      console.log(
        "about to send message " +
          messageTextRef.current +
          " " +
          currentChannelId,
      );
      const message = messageTextRef.current;
      await API.post(
        `/board-select/board/${boardMetaInfo.boardId}/channel/${currentChannelId}/message`,
        { message: message },
      );
      //setAllMessages([...allMessages,{writer: "Anon", message: messageTextRef.current}]) //demodata
      console.log("message sent");
      // console.log(response);
      messageTextRef.current = "";
      await refreshChatBox();
    }

    (
      document.getElementsByName("usermessage") as NodeListOf<HTMLElement>
    )[0].innerHTML = "";
  };

  const handleRemoveMessage = async (messageId: ObjectID) => {
    try {
      await API.del(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${currentChannelId}/message/${messageId.toString()}`,
      );
      await refreshChatBox();
    } catch (error) {
      console.log("remove message from channel error " + error);
    }
  };

  const handlePinMessage = async (messageId: ObjectID) => {
    try {
      const response = await API.put(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${currentChannelId}/message/${messageId.toString()}/pin`,
      );
      console.log("response from pin " + response);
      await refreshChatBox(currentChannel?._id, 1);
      await refreshChannel(currentChannel?._id);
    } catch (error) {
      console.log("pin meesage error " + error);
    }
  };

  const handleUnpinMessage = async (messageId: ObjectID) => {
    try {
      const response = await API.del(
        `/board-select/board/${
          boardMetaInfo.boardId
        }/channel/${currentChannelId}/message/${messageId.toString()}/pin`,
      );
      console.log(response);
      await refreshChatBox(currentChannel?._id, 1);
      await refreshChannel(currentChannel?._id);
    } catch (error) {
      console.log("unpin message error " + error);
    }
  };

  const handleStartPrivateMessage = async (userToMessage: IUser) => {
    console.log("about to start private message");
    setUserSelected(userToMessage);
    setShowPrivateMessage(true);
    console.log(userSelected + " " + setShowPrivateMessage);
  };

  const boardChatBoxProp: IBoardChatBoxProp = {
    activeChannel: currentChannelName,
    boardMetaInfo: boardMetaInfo,
    messages: allMessages,
    handleInputChange: handleInputChange,
    handleSendMessage: handleSendMessage,
    handleRemoveMessage: handleRemoveMessage,
    handlePinMessage: handlePinMessage,
    userMessage: messageTextRef,
    setManagerPageType: setManagerPageType,
  };
  const sideBarProp: IBoardSideBarProp = {
    managerPageType: managerPageType,
    boardMetaInfo: boardMetaInfo,
    handleRefreshSidebar: refreshSideBar,
    handleRefreshChatBox: refreshChatBox,
    setManagerPageType: setManagerPageType,
    handleSwitchChannel: handleSwitchChannel,
    currentChannel: currentChannel,
    setCurrentChannel: setCurrentChannel,
    handleRemoveBoardMember: handleRemoveMemberBoard,
    handleRemoveChannel: handleRemoveChannel,
    handleStartPrivateMessage: handleStartPrivateMessage,
  };
  // const demoPopupProp : IBoardManagerPopupProp = {managerPageType: managerPageType, setManagerPageType: setManagerPageType, channelToMange: "Documents"};
  //const demoSearchTextProp: ISearchMessageProp = {currentChannel: currentChannel,handleSearchText:handleSearchMessage,hasMessageFound:hasMessageFound,managerPageType:managerPageType,
  //    messagesFound:messagesFound, setManagerPageType:setManagerPageType,messageQueryRef:messageQueryRef};
  const handlersProp: IBoardManagerPopupProp = {
    managerPageType: managerPageType,
    setManagerPageType: setManagerPageType,
    handleAddOperation: handleCreateChannel,
    handleRemoveOperation: handleRemoveChannel,
    channelToMange: currentChannel,
    handleRenameOperation: handleRenameChannel,
    handleAddMemberChannelOperation: handleAddMemberChannel,
    handleRemoveMemberChannelOperation: handleRemoveMemberChannel,
    refreshChannel: refreshChannel,
    handleSearchText: handleSearchMessage,
    hasMessageFound: hasMessageFound,
    messagesFound: messagesFound,
    handleUnpinMessage: handleUnpinMessage,
    handleInputChange: handleInputChange,
  };
  // const privateChatProp: IPrivateChatProp = {boardID: boardMetaInfo.boardId, userToMessage: userSelected}

  return (
    <>
      <div className="container__board">
        <table className="table__boardlayout">
          <tbody>
            <tr>
              <th>
                <BoardSideBar {...sideBarProp} {...handlersProp} />
              </th>
              <th>
                <BoardChatBox {...boardChatBoxProp} />
              </th>
            </tr>
          </tbody>
        </table>

        {showPrivateMessage && userSelected && (
          <DMChatBox
            {...{
              boardID: boardMetaInfo.boardId,
              userToMessage: userSelected,
              setShowPrivateMessage: setShowPrivateMessage,
            }}
          />
        )}

        {managerPageType == ManagerPageType.ManageBoardMembers && (
          <BoardManagerPopup
            {...handlersProp}
            handleAddOperation={handleAddMemberBoard}
            handleRemoveOperation={handleRemoveMemberBoard}
          />
        )}
        {managerPageType == ManagerPageType.MangeChannels && (
          <BoardManagerPopup
            {...handlersProp}
            handleAddOperation={handleCreateChannel}
            handleRemoveOperation={handleRemoveChannel}
          />
        )}
        {managerPageType == ManagerPageType.ManageChannelMembers && (
          <BoardManagerPopup
            {...handlersProp}
            channelToMange={currentChannel}
            handleAddMemberChannelOperation={handleAddMemberChannel}
            handleRemoveMemberChannelOperation={handleRemoveMemberChannel}
          />
        )}
        {managerPageType == ManagerPageType.SearchText && (
          <BoardManagerPopup
            {...handlersProp}
            channelToMange={currentChannel}
            handleSearchText={handleSearchMessage}
          />
        )}
        {managerPageType == ManagerPageType.RenameChannel && (
          <BoardManagerPopup
            {...handlersProp}
            handleRenameOperation={handleRenameChannel}
          />
        )}
        {managerPageType == ManagerPageType.ShowPinMessage && (
          <BoardManagerPopup {...handlersProp} />
        )}
      </div>
    </>
  );
};

export default Board;
