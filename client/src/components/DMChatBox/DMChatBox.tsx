import { IPrivateChatProp } from "@/interfaces";
import { IMessageWithObjects } from "@/types/message/message";
import { useEffect, useRef, useState } from "react";
//import ChatMessage from "../ChatMessage";
import { API } from "@/utils/requests/API";
import './DMChatBox.css'

const DMChatBox: React.FC<IPrivateChatProp> = (privateChatProp: IPrivateChatProp) => {
    const textToSend = useRef("");
    const [allDMMessages, setAllDMMessages] = useState<IMessageWithObjects[]>([]);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { value } = event.target;
        textToSend.current = value

    };

    const refreshPrivateMessage = async () => {
        try {
            console.log("refresh channel");
            const obj = await API.get(`/board-select/board/${privateChatProp.boardID}/member/${privateChatProp.userToMessage._id}/message`);
            console.log(obj)
            if(obj.data) {
                const messages: IMessageWithObjects[] = obj.data as IMessageWithObjects[];
                setAllDMMessages(messages)
            } else {
                setAllDMMessages([])
            }

        } catch(error) {
            console.log("refresh private message error " + error);
        }
    }

    const handleSendPrivateMessage = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();        

        if (textToSend.current != "") {
            // Logic for sending message api call
            console.log("about to send private message " + textToSend.current + " " + privateChatProp.userToMessage.username);
            const message = textToSend.current;
            const obj = await API.post(`/board-select/board/${privateChatProp.boardID}/member/${privateChatProp.userToMessage._id}/message`, {message: message});
            //setAllMessages([...allMessages,{writer: "Anon", message: messageTextRef.current}]) //demodata
            console.log("message sent" + obj)
            // console.log(response);
            textToSend.current = ""
            await refreshPrivateMessage();
        }

        (event.currentTarget.elements.namedItem('usermessage') as HTMLTextAreaElement).value = "";
    }

    useEffect(() => {
        refreshPrivateMessage();
      }, [])

    return (
        <div className="container__privatechat">
            
            <div className="container__username">
                <h4>Messaging to: {privateChatProp.userToMessage.username}</h4>
	    </div>
	

	    <div className="container__buttons">
                <button className="fancyButton" onClick={refreshPrivateMessage} >
			<img src="/refresh.png" alt="Refresh icon" style={{ width: '40px', height: 'auto' }} />
		</button> 
                <button className="fancyButton" >
			<img src="/search-icon-red.png" alt="Search icon" style={{ width: '40px', height: 'auto' }} />
		</button>
                <button className="fancyButton" onClick={() => privateChatProp.setShowPrivateMessage(false)}>
			X
		</button>
            </div>

            {/* <div>pinned Message</div> <div>search</div> */}
            <div className="container__privatemessagesdisplay" id="chat-messages">
                {(allDMMessages.map((message) => <p>{message.user.username} : {message.content}</p>))}
                <div id="endofmessage"></div>

            </div>

            <div className="container__privatemessagesender">
                <form onSubmit={handleSendPrivateMessage}>
                    <textarea name="usermessage" className="textarea__privatewritemessage" onChange={handleInputChange}/>
                    {/* <select name="emoji" id="emoji">:)</select> */}
                    <input type="submit" value="Send DM"/>
                </form>
            </div>

        </div>
    )
}

export default DMChatBox;
