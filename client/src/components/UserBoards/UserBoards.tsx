// The component for the "Your Discussion Boards" page

import { useEffect, useState } from "react";
import UserBoardList from "../UserBoardList/UserBoardList";
import "./UserBoards.css";
import { API } from "../../utils/requests/API";
import "./pop-up.css";
import { getUserBoards } from "@/utils/requests/boardsUtil/selectBoards";
import { IBoardWithObjects } from "@/types/board";
import { useBoxAnimation } from "@/utils/boxAnimation";
import ObjectID from "bson-objectid";
import { useNavigate } from "react-router-dom";

const UserBoards: React.FC = () => {
  const navigate = useNavigate();

  const { clearImages } = useBoxAnimation();
  const [boardList, setBoardList] = useState<IBoardWithObjects[]>([]);
  const [displayPopup, setPopup] = useState(false);
  const [board, setBoard] = useState("");

  const fetchUserBoardList = async () => {
    try {
      const boardsObj = await getUserBoards();
      setBoardList(boardsObj.data ?? []);
    } catch (error) {
      console.log("error when fetching user boardlist " + error);
    }
  };

  useEffect(() => {
    console.log("Fetched user data");
    fetchUserBoardList().then(() => console.log(boardList));
    return clearImages;
  }, []);

  const closePopup = () => {
    setPopup(false);
  };

  const openPopup = () => {
    setPopup(true);
  };

  const createBoard = async () => {
    // calling backend API - this part doesn't work; error in console

    try {
      const attempt = await API.post("/board-select/board", { name: board });
      console.log(attempt);
      await fetchUserBoardList();
      closePopup();
    } catch (error) {
      console.error("Board not created: ", error);
    }
  };

  const deleteBoard = async (boardId: ObjectID) => {
    try {
      await API.del(`/board-select/board/${boardId}`);
      console.log("Successfully deleted");
      await fetchUserBoardList();
    } catch (error) {
      console.error("Deletion error: ", error);
    }
  };

  const selectBoard = async (boardId: ObjectID) => {
    try {
      const obj = await API.get<IBoardWithObjects>(
        `/board-select/board/${boardId}`,
      );
      console.log(obj.success);
      console.log(obj);
      if (obj.success) {
        const boardObject: IBoardWithObjects = obj.data as IBoardWithObjects;
        console.log("Successfully selected");
        console.log(boardObject);
        navigate("/boards/board", { state: { board: boardObject } });
      }
    } catch (error) {
      console.error("Select error: ", error);
    }
  };

  return (
    <>
      <div className="container__userboards">
        <div className="container__userboardstop">
          <div className="fancyBox">
            <p>Discussion Boards</p>
          </div>
          <button className="fancyButton" onClick={openPopup}>
            Create Board
          </button>
          <button className="fancyButton2" onClick={fetchUserBoardList}>
            Refresh
          </button>
        </div>

        {boardList ? (
          <UserBoardList
            boardList={boardList}
            deleteBoard={deleteBoard}
            selectBoard={selectBoard}
          />
        ) : (
          <p>Error when fetching boardlist data</p>
        )}
      </div>

      {displayPopup && (
        <div className="pop-up">
          <div className="pop-up-fields">
            <h2>Create New Board</h2>
            <label>Board Name:</label>
            <input
              type="text"
              value={board}
              onChange={(e) => setBoard(e.target.value)}
            />
            <button className="fancyButton" onClick={createBoard}>
              Create Board
            </button>
            <button className="fancyButton2" onClick={closePopup}>
              Cancel
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default UserBoards;
