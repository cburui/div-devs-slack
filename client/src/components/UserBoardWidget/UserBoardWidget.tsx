//import { useNavigate } from "react-router-dom";
import "./UserBoardWidget.css";
import { IBoardWithObjects } from "@/types/board";
import ObjectID from "bson-objectid";

export interface IUserBoardWidgetInfo {
  boardName: string;
  creator: string;
  boardId: string;
}

interface IUserBoardWidgetProps {
  boardInfo: IBoardWithObjects;
  deleteBoard?: (boardId: ObjectID) => Promise<void>;
  selectBoard?: (boardId: ObjectID) => Promise<void>;
}

const UserBoardWidget: React.FC<IUserBoardWidgetProps> = ({
  boardInfo,
  deleteBoard,
  selectBoard,
}) => {
  //const navigate = useNavigate();

  // const onBoardSelect = (event: React.MouseEvent<HTMLDivElement>) => {
  //     event.preventDefault();
  //     navigate('/boards/board',{state:{name:"Testing passing prop from widget"}});
  // }

  const deleteButton = async (event: React.MouseEvent<HTMLDivElement>) => {
    //handle deletion
    event.stopPropagation();
    await deleteBoard!(boardInfo._id);
  };

  const onBoardSelect = async (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    await selectBoard!(boardInfo._id);
  };

  return (
    <div className="container__userboardwidget" onClick={onBoardSelect}>
      <div className="container__board--main">
        <h3>{boardInfo.name}</h3>

        <div className="delete-icon" onClick={deleteButton}>
          <img src="/delete-icon.png" alt="Delete Icon" />
        </div>
      </div>

      <div className="container__board--extra">
        <ul className="list__boardinfo">
          <li>Creator: {boardInfo.admin.username}</li>
          <li>BoardId : {boardInfo._id.toString()}</li>
        </ul>
      </div>
    </div>
  );
};

export default UserBoardWidget;
