// The component for searching message and displaying the result messages

import { ISearchMessageProp, ManagerPageType } from "@/interfaces";
// import ChatMessage from "../ChatMessage";
import "./MessageSearchBox.css";

const MessageSearchBox: React.FC<ISearchMessageProp> = (
  messageSearchProp: ISearchMessageProp,
) => {
  // const textRef = useRef("");

  return (
    <>
      <div className="container__messagesearch">
        <button
          onClick={(_) =>
            messageSearchProp.setManagerPageType(ManagerPageType.None)
          }
        >
          X
        </button>
        <div className="container__textsearchform">
          <form
            className="form__textsearch"
            onSubmit={() =>
              messageSearchProp.handleSearchText(
                messageSearchProp.currentChannel!._id,
                1,
                messageSearchProp.messageQueryRef.current,
              )
            }
          >
            <input
              className="input__textsearch"
              onChange={(e) =>
                (messageSearchProp.messageQueryRef.current = e.target.value)
              }
            />
            <input type="submit" name="Search Message" />

            {/* <button className="button__textsearch" onClick={(_) => messageSearchProp.handleSearchText(textRef.current)}>
                        Search Message
                    </button> */}
          </form>
        </div>
        <div className="container__textlist" id="textlist-main">
          {/* {messageSearchProp.hasMessageFound && messageSearchProp.messagesFound.length > 0 && messageSearchProp.messagesFound.map((message) => <ChatMessage {...message} /> )} */}
        </div>
      </div>
    </>
  );
};

export default MessageSearchBox;
