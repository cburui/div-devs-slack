import {
  IBoardManagerPopupProp,
  IBoardSideBarProp,
  ManagerPageType,
} from "@/interfaces";
import "./BoardSideBar.css";
import { useUserContext } from "@/utils/context/user";

const BoardSideBar: React.FC<IBoardSideBarProp & IBoardManagerPopupProp> = (
  sidebarProp: IBoardSideBarProp & IBoardManagerPopupProp,
) => {
  // const [currentChannel, setCurrentChannel] = useState("General");

  // const handleSwitchChannel = (event: React.MouseEvent<HTMLDivElement>, channelName:string) => {
  //     event.preventDefault();
  //     setCurrentChannel(channelName);
  // 	sidebarProp.handleSwitchChannel(event, channelName);
  // };
  //console.log("inside sidebar, chaneels are " + sidebarProp.boardMetaInfo.channels.map((c) => c.name));
  const {
    user: { username },
  } = useUserContext();

  return (
    <>
      <div className="container__boardsidebar">
        <div className="container__boardname">
          <h2>{sidebarProp.boardMetaInfo.boardName}</h2>

          <div className="container__sidebaritem">
            <div className="container__baritembutton">User: {username}</div>
            {/* {sidebarProp.boardMetaInfo.members.map((member) => <div className="container__baritembutton">User: fix anon</div>)} */}
            {/* <div className="container__baritembutton">Channel: fix this{}</div> */}
          </div>

          <button
            className="fancyButton"
            onClick={sidebarProp.handleRefreshSidebar}
          >
            Refresh Sidebar
          </button>
          {/* <button */}
            {/* className="fancyButton2" */}
            {/* onClick={sidebarProp.handleRefreshChatBox} */}
          {/* > */}
            {/* Refresh Chat */}
          {/* </button> */}
        </div>

        <div className="container__sidebaritem">
          <button
            className="fancyButton2"
            onClick={() =>
              sidebarProp.setManagerPageType(ManagerPageType.MangeChannels)
            }
          >
            Manage Channels
          </button>
          <button
            className="fancyButton"
            onClick={() =>
              sidebarProp.setManagerPageType(ManagerPageType.ManageBoardMembers)
            }
          >
            Manage Members
          </button>
          <br />
          <br />
          {/* <div className="container__baritembutton">Channel: fix this{sidebarProp.activeChannel}</div> */}
          <div>To refresh the channel, press on the channel again :)</div>
        </div>

        <div className="container__category">
          <h4>All Channels</h4>

          {sidebarProp.boardMetaInfo.channels.map((channel) => (
            <div
              key={channel._id.toString()}
              className="fancyButton2 container__channelitem"
              onClick={() =>
                sidebarProp.handleSwitchChannel(channel._id, channel)
              }
            >
              {channel._id === sidebarProp.currentChannel?._id && (<button
                className="fancyButton"
                onClick={() => {
                  sidebarProp.handleSwitchChannel(channel._id, channel);
                  sidebarProp.setManagerPageType(
                    ManagerPageType.ManageChannelMembers,
                  );
                }}
              >
                |||
              </button>)}
              <div
                onClick={() =>
                  sidebarProp.handleSwitchChannel(channel._id, channel)
                }
              >
                {channel.name}
              </div>
              {channel._id === sidebarProp.currentChannel?._id &&
                sidebarProp.boardMetaInfo.userIsAdmin && (
                  <button
                    className="fancyButton"
                    onClick={() => {
                      sidebarProp.handleSwitchChannel(channel._id, channel);
                      sidebarProp.setManagerPageType(
                        ManagerPageType.RenameChannel,
                      );
                      console.log("rename " + sidebarProp.managerPageType);
                    }}
                  >
                    rename
                  </button>
                )}
              {/* <button className="fancyButton" onClick={() => sidebarProp.handleRenameOperation(channel._id, newChannelName.current)}>rename</button> */}
              {sidebarProp.boardMetaInfo.userIsAdmin && channel.name !== "general" && (
                <div
                  className="delete-icon"
                  onClick={() => sidebarProp.handleRemoveChannel(channel._id)}
                >
                  <img src="/delete-icon.png" alt="Delete Icon" />
                </div>
              )}
            </div>
          ))}
        </div>

        <div className="container__category">
          <h4>All Members</h4>

          {sidebarProp.boardMetaInfo.members.map((member) => (
            <div
              key={member.email}
              className="fancyButton2 container__channelitem"
            >
              {/* <button className="fancyButton" onClick={() => sidebarProp.setManagerPageType(ManagerPageType.ManageChannelMembers)}>|||</button> */}
              <div>{member.username}</div>
              <button
                className="fancyButton"
                onClick={() => sidebarProp.handleStartPrivateMessage(member)}
              >
                message
              </button>
              {sidebarProp.boardMetaInfo.userIsAdmin && (
                <div
                  className="delete-icon"
                  onClick={() =>
                    sidebarProp.handleRemoveBoardMember(member._id)
                  }
                >
                  <img src="/delete-icon.png" alt="Delete Icon" />
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default BoardSideBar;
