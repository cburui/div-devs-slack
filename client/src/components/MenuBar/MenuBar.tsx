// import { useState } from "react";
// import { PageType, IPageTypeManager } from "../../types/pageType";
import { useUserContext } from "@/utils/context/user";
import "./Menubar.css";
import { useNavigate } from "@/router";

const MenuBar: React.FC = () => {
  const navigate = useNavigate();
  const {
    actions: { logout },
  } = useUserContext();

  const handleLogout = async (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    await logout();
    navigate("/");
  };
  return (
    <>
      <div className="container__menubar">
        <div className="container__menuitems--left">
          <img
            src="/McGuild_Sword.png"
            alt="Logo"
            style={{
              width: "200px",
              marginLeft: "10px",
              height: "auto",
              animation: "none",
            }}
          />
          {/*<button>LOGO</button>*/}
        </div>

        <div className="container__menuitems--middle">
          <button onClick={() => navigate("/boards/userboards")}>
            Your Boards
          </button>
          {/*<button >Explore</button>*/}
          {/*<button >Notifications</button>*/}
        </div>

        <div className="container__menuitems--right">
          <button onClick={handleLogout}>Logout</button>
        </div>
      </div>
    </>
  );
};

export default MenuBar;
