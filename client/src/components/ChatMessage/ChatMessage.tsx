// the mini component for displaying each of the individual messages. Basically it's trying to combine treating sender's
// name, message itself and potentially user avatar as a single unit.

import { IBoardChatBoxProp } from "@/interfaces";
import "./ChatMessage.css";
import { useState } from "react";
import { IMessageWithObjects } from "@/types/message/message";

const ChatMessage: React.FC<IMessageWithObjects & IBoardChatBoxProp> = (
  messageInfo: IMessageWithObjects & IBoardChatBoxProp,
) => {
  const [isShowOption, setIsShowOption] = useState(false);

  return (
    <>
      <div
        className="container__chatmessage"
        onMouseOver={() => setIsShowOption(true)}
        onMouseOut={() => setIsShowOption(false)}
      >
        <p className="p__messageauthor">{messageInfo.user.username}: </p>
        <div className="container__messagebox">
          <p className="p__messagecontent">{messageInfo.content}</p>
        </div>

        {isShowOption && (
          <button
            onClick={() => messageInfo.handleRemoveMessage(messageInfo._id)}
          >
            delete
          </button>
        )}
        {isShowOption && (
          <button onClick={() => messageInfo.handlePinMessage(messageInfo._id)}>
            pin
          </button>
        )}
      </div>
    </>
  );
};

export default ChatMessage;
