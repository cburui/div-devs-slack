// This component is mostly a wrapper class for the UserBoardWidget(s). It fetches data from the server and create a list of UserBoardWidgets
// TODO need to implement the call to backend

//import { useState } from "react"
import ObjectID from "bson-objectid";
import UserBoardWidget from "../UserBoardWidget/UserBoardWidget";
import "./UserBoardList.css";
import { IBoardWithObjects } from "@/types/board";

type UserBoardListProps = {
  boardList: IBoardWithObjects[];
  deleteBoard: (boardId: ObjectID) => Promise<void>;
  selectBoard: (boardId: ObjectID) => Promise<void>;
};

const UserBoardList: React.FC<UserBoardListProps> = ({
  boardList,
  deleteBoard,
  selectBoard,
}: UserBoardListProps) => {
  //const [isLoading, changeIsLoading] = useState<boolean>(true)

  // Some logic for fetching data
  // get a list of boards

  // changeIsLoading(false)

  // const demoUserboardwidgets: IUserBoardWidgetInfo[] = [{boardName: "COMP327", creator: "JaneDoe", boardId: "123123"},{boardName: "COMP307", creator: "JaneDoe", boardId: "123123"},{boardName: "COMP307", creator: "JaneDoe", boardId: "123123"},{boardName: "COMP307", creator: "JaneDoe", boardId: "123123"},{boardName: "COMP307", creator: "JaneDoe", boardId: "123123"},{boardName: "COMP361", creator: "JohnDoe", boardId: "321321"}];
  return (
    <>
      <div className="container__userboardlist" id="userboardlist-main">
        <div>
          {/*isLoading && <button onClick={() => changeIsLoading(false)}>Press to load demo data</button>*/}
          {/*isLoading && <h3 className="h3__loading-screen">LOADING...</h3>*/}
        </div>

        {boardList &&
          boardList.length > 0 &&
          boardList.map((widgetInfo) => (
            <UserBoardWidget
              key={widgetInfo._id.toString()}
              deleteBoard={deleteBoard}
              selectBoard={selectBoard}
              boardInfo={widgetInfo}
            />
          ))}
      </div>
    </>
  );
};

export default UserBoardList;
