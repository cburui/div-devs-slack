// The component that corresponds to the right side of inside of a discussion board
// includes the display of all messages and input text area

import ChatMessage from "../ChatMessage";
import { IBoardChatBoxProp, ManagerPageType } from "@/interfaces";
import "./BoardChatBox.css";

const BoardChatBox: React.FC<IBoardChatBoxProp> = (
  chatBoxProp: IBoardChatBoxProp,
) => {
  // Initialize the chat history or something
  // const [allMessages, setAllMessages] = useState<IChatMessageInfo[]>([{writer: "user1", message: "Hi"}]);
  // const messageTextRef = useRef<string>("")

  // const handleInputChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
  //     const { value } = event.target;
  //     messageTextRef.current = value

  //   };

  // const handleSendMessage = (event: React.FormEvent<HTMLFormElement>) => {
  //     event.preventDefault();
  //     (event.currentTarget.elements.namedItem('usermessage') as HTMLTextAreaElement).value = "";

  //     if (messageTextRef.current != "") {
  //         setAllMessages([...allMessages,{writer: "user1", message: messageTextRef.current}])
  //     }

    // }

  return (
    <>
      <div className="container__boardchatbox">
        <div className="container__channeltitle">
          <h4>Channel: {chatBoxProp.activeChannel}</h4>
	</div>
	
	<div className="container__buttons">
          <button className="fancyButton"
            onClick={(_) =>
              chatBoxProp.setManagerPageType(ManagerPageType.ShowPinMessage)
            }
          >
		<img src="/pinned-icon-red.png" alt="Pin icon" style={{ width: '40px', height: 'auto' }} />
          </button>
          <button className="fancyButton" onClick={(_) =>
              chatBoxProp.setManagerPageType(ManagerPageType.SearchText)} >
          	<img src="/search-icon-red.png" alt="Search icon" style={{ width: '40px', height: 'auto' }} />	
	  </button>
        </div>
        {/* <div>pinned Message</div> <div>search</div> */}
        <div className="container__messagesdisplay" id="chat-messages">
          {chatBoxProp.messages.map((message) => (
            <ChatMessage
              key={message._id.toString()}
              {...message}
              {...chatBoxProp}
            />
          ))}
          <div id="endofmessage"></div>
        </div>

        <div className="container__messagesender">
          <form onSubmit={chatBoxProp.handleSendMessage}>
            <textarea
              name="usermessage"
              className="textarea__writemessage"
              onChange={chatBoxProp.handleInputChange}
            />
            {/* <select name="emoji" id="emoji">:)</select> */}
            <input type="submit" value="Send" />
          </form>
        </div>
      </div>
    </>
  );
};

export default BoardChatBox;
