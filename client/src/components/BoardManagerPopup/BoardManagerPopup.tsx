import { IBoardManagerPopupProp, ManagerPageType } from "@/interfaces";
import { useRef, useState } from "react";
import "./BoardManagerPopup.css";
import ObjectID from "bson-objectid";

const BoardManagerPopup: React.FC<IBoardManagerPopupProp> = (
  managerProp: IBoardManagerPopupProp,
) => {
  const objectToManageRef = useRef<string>("");
  const operationName = useRef<string>(""); // add for adding members and remove for remove members
  const [isValidInput, setIsValidInput] = useState(true);

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ref: React.MutableRefObject<string>,
  ) => {
    const { value } = event.target;
    ref.current = value;
  };

  const handleFormSubmit = async (
    event: React.FormEvent<HTMLFormElement>,
    managerType: ManagerPageType,
  ) => {
    console.log("handle formsubmit");
    event.preventDefault();
    //managerProp.handleRenameOperation(managerProp.channelToMange!._id, objectToManageRef.current);
    if (
      managerType === ManagerPageType.RenameChannel &&
      objectToManageRef.current != ""
    ) {
      console.log("rename channel operation");
      managerProp.handleRenameOperation(
        managerProp.channelToMange!._id,
        objectToManageRef.current,
      );
    } else if (
      (managerType === ManagerPageType.MangeChannels ||
        managerType === ManagerPageType.ManageBoardMembers) &&
      objectToManageRef.current != ""
    ) {
      console.log("add channel or board member");
      managerProp.handleAddOperation(objectToManageRef.current);
    } else if (
      managerType === ManagerPageType.ManageChannelMembers &&
      objectToManageRef.current != ""
    ) {
      console.log("manage member add");
      console.log(objectToManageRef.current);
      managerProp.handleAddMemberChannelOperation(
        managerProp.channelToMange!._id,
        objectToManageRef.current,
      );
    } else if (
      managerType === ManagerPageType.SearchText &&
      objectToManageRef.current != ""
    ) {
      console.log("search text operation");
      managerProp.handleSearchText(
        managerProp.channelToMange!._id,
        1,
        objectToManageRef.current,
      );

    } else if (
      operationName.current === "remove" &&
      objectToManageRef.current != ""
    ) {
      managerProp.handleRemoveOperation(
        ObjectID.createFromHexString(objectToManageRef.current),
      );
    } else {
      setIsValidInput(false);
      return;
    }
    await managerProp.refreshChannel();
    // managerProp.setManagerPageType(ManagerPageType.None);
    // operationName.current = "";
    // objectToManageRef.current = "";
  };

  return (
    <>
      <div className="container__managerpopup">
        <div>
          <button
            className="fancyButton2"
            onClick={() => managerProp.setManagerPageType(ManagerPageType.None)}
          >
            X
          </button>
        </div>
        {managerProp.managerPageType == ManagerPageType.ManageBoardMembers && (
          <form
            onSubmit={(e) =>
              handleFormSubmit(e, ManagerPageType.ManageBoardMembers)
            }
          >
            <label> Manage board members</label> <br></br>
            <label>Member Email:</label> <br></br>
            <input
              type="text"
              onChange={(e) => handleInputChange(e, objectToManageRef)}
            />{" "}
            <br></br>
            {/* <label>"add" or "remove"</label> <br></br> 
                <input type="text" onChange={(e) => handleInputChange(e, operationName)}/> <br></br> */}
            <input className="fancyButton" type="submit" value="Submit" />
          </form>
        )}

        {managerProp.managerPageType == ManagerPageType.MangeChannels && (
          <form
            onSubmit={(e) => handleFormSubmit(e, ManagerPageType.MangeChannels)}
          >
            <label> Manage channels</label> <br></br>
            <label>Channel Name:</label> <br></br>
            <input
              type="text"
              onChange={(e) => handleInputChange(e, objectToManageRef)}
            />{" "}
            <br></br>
            <input className="fancyButton" type="submit" value="Submit" />
          </form>
        )}

        {/* {managerProp.managerPageType == ManagerPageType.ManageChannelMembers && <div>manage channel members</div> } */}

        {managerProp.managerPageType == ManagerPageType.ManageChannelMembers &&
          managerProp.channelToMange && (
            <form
              onSubmit={(e) =>
                handleFormSubmit(e, ManagerPageType.ManageChannelMembers)
              }
            >
              <label>
                {" "}
                Manage members of {managerProp.channelToMange.name}
              </label>{" "}
              <br></br>
              <label>Member Email:</label> <br></br>
              <input
                type="text"
                onChange={(e) => handleInputChange(e, objectToManageRef)}
              />{" "}
              <br></br>
              {/* <label>"add" or "remove"</label> <br></br> 
                <input type="text" onChange={(e) => handleInputChange(e, operationName)}/> <br></br> */}
              <input className="fancyButton" type="submit" value="Submit" />
            </form>
          )}

        {/* Rename Channel Functionality */}
        {managerProp.managerPageType == ManagerPageType.RenameChannel &&
          managerProp.channelToMange && (
            <form
              onSubmit={(e) =>
                handleFormSubmit(e, ManagerPageType.RenameChannel)
              }
            >
              <label>Rename channel of "{managerProp.channelToMange.name}"</label>{" "}
              <br></br>
              <label>New Channel Name:</label> <br></br>
              <input
                type="text"
                onChange={(e) => handleInputChange(e, objectToManageRef)}
              />{" "}
              <br></br>
              <input className="fancyButton" type="submit" value="Submit" />
            </form>
          )}

        {/* For displaying the list of members in that channel */}
        {managerProp.managerPageType ==
          ManagerPageType.ManageChannelMembers && (
          <div>
            <div>Number of members in the channel: {managerProp.channelToMange!.users.length}</div>
            {managerProp.channelToMange?.users.map((user) => (
              <div
                key={user.email}
                className="fancyButton2 container__channelitem"
              >
                {/* <button className="fancyButton" onClick={() => sidebarProp.setManagerPageType(ManagerPageType.ManageChannelMembers)}>|||</button> */}
                <div>{user.username}</div>
                <div>{user.email}</div>
                <button
                  className="fancyButton"
                  onClick={() => {
                    console.log("try remove user from channel " + user._id);
                    managerProp.handleRemoveMemberChannelOperation(
                      managerProp.channelToMange!._id,
                      user._id,
                    );
                  }}
                >
                  remove member
                </button>
              </div>
            ))}
          </div>
        )}

        {managerProp.managerPageType == ManagerPageType.ShowPinMessage &&
          managerProp.channelToMange && (
            <div
              onClick={() =>
                console.log(managerProp.channelToMange!.pinnedMessages)
              }
            >
              pinned {managerProp.channelToMange!.pinnedMessages.length}
            </div>
          )}
        {managerProp.managerPageType == ManagerPageType.ShowPinMessage &&
          managerProp.channelToMange && (
            <div>
              {managerProp.channelToMange.pinnedMessages.map((message) => (
                <div>
                  <p>{message.user.username}:</p>
                  <p>
                    {message.content}
                    <button
                      className="fancyButton"
                      onClick={() =>
                        managerProp.handleUnpinMessage(message._id)
                      }
                    >
                      Unpin
                    </button>
                  </p>
                </div>
              ))}
            </div>
          )}

        {managerProp.managerPageType === ManagerPageType.SearchText &&
          managerProp.channelToMange && (
            <>
              <form
                onSubmit={(e) =>
                  handleFormSubmit(e, ManagerPageType.SearchText)
                }
              >
                <label>Search Messages</label> <br></br>
                <label>Enter text:</label> <br></br>
                <input
                  type="text"
                  onChange={(e) => handleInputChange(e, objectToManageRef)}
                />{" "}
                <br></br>
                <input className="fancyButton" type="submit" value="Submit" />
              </form>
              <div>
                {managerProp.hasMessageFound ? (
                  managerProp.messagesFound.map((message) => (
                    <div>
                      <p>{message.user.username}:</p>
                      <p>{message.content}</p>
                    </div>
                  ))
                ) : (
                  <p>No messages found</p>
                )}
              </div>
            </>
          )}

        {!isValidInput && <p>Input invalid. Please retry.</p>}
      </div>
    </>
  );
};

export default BoardManagerPopup;
