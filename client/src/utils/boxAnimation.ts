import { useEffect, useState } from "react";

export const useBoxAnimation = () => {
  const [imgCount, setImgCount] = useState(0);
  // spawns an initial 5 images on screen
  const spawnInitial = () => {
    for (let i = 0; i < 4; i++) {
      spawnImage();
    }
    setImgCount(4);
  };

  const getRandomInterval = () => {
    return Math.floor(Math.random() * 4000) + 1000;
  };

  // spawns new image on screen
  const spawnImage = () => {
    const imgSpawn = createImg(getName());
    setRandomPosition(imgSpawn);
    setRandomSize(imgSpawn);
    document.body.appendChild(imgSpawn);
  };

  // creates a new image to be spawned
  const createImg = (imgName: string) => {
    const imgSpawn = document.createElement("img");
    imgSpawn.src = imgName;
    imgSpawn.alt = "text box error";
    imgSpawn.classList.add("textBox");
    return imgSpawn;
  };

  // 50/50 text or alt box image
  const getName = () => {
    const random = Math.random();
    if (random < 0.5) {
      return "/text-box.png";
    } else {
      return "/alt-box.png";
    }
  };

  // sets random x/y
  const setRandomPosition = (element: HTMLElement) => {
    element.style.left = getRandomPosition(window.innerWidth);
    element.style.top = getRandomPosition(window.innerHeight);
  };

  // returns random position within parameter maxWidth
  const getRandomPosition = (maxWidth: number) => {
    return Math.random() * (maxWidth - 100) + "px";
  };

  // sets size of text-box, with fixed w/h
  const setRandomSize = (element: HTMLElement) => {
    element.style.width = getRandomSize();
    element.style.height = element.style.width;
  };

  // returns random size in px
  const getRandomSize = () => {
    return Math.floor(Math.random() * 100) + "px";
  };

  const clearImages = () => {
    document.querySelectorAll("img.textBox").forEach((img) => img.remove());
  };

  // initiate spawning sequence... !
  useEffect(() => {
    const maxImg = 35;
    if (imgCount === 0) {
      spawnInitial();
    }
    const interval = setInterval(function () {
      if (imgCount < maxImg) {
        spawnImage();
        setImgCount(imgCount + 1);
      }
      clearInterval(interval);
    }, getRandomInterval());
    return () => {
      clearInterval(interval);
    };
  }, [imgCount]);

  return { clearImages };
};
