import { ILoginPayload, ILoginResponse } from "@/types/user";
import { SuccessResponse } from "@/types/util";
import { API } from "@/utils/requests/API";

export const login = async (
  payload: ILoginPayload,
): Promise<SuccessResponse<ILoginResponse>> => {
  return await API.post("/login", payload);
};
