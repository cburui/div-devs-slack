import { SuccessResponse } from "@/types/util";
import { API } from "@/utils/requests/API";

export const logout = async (): Promise<SuccessResponse<void>> => {
  return await API.del("/user/logout");
};
