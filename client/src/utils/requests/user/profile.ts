import { IPublicUser } from "@/types/user";
import { API } from "../API";
import { SuccessResponse } from "@/types/util";

export const getProfileInfo = async (): Promise<
  SuccessResponse<IPublicUser>
> => {
  return await API.get("/user/profile");
};
