import { IRegisterPayload } from "@/types/user";
import { SuccessResponse } from "@/types/util";
import { API } from "@/utils/requests/API";

export const register = async (
  payload: IRegisterPayload,
): Promise<SuccessResponse<void>> => {
  return await API.post("/register", payload);
};
