import { SuccessResponse } from "@/types/util/index";
type HttpMethod = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";

let instance: Api | null = null;

class Api {
  baseURL?: string = import.meta.env.VITE_BACKEND_URL;

  constructor() {
    if (instance) {
      return instance;
    }
    instance = this;
  }

  async request<T>(
    url: string,
    method: HttpMethod,
    body?: object,
    headers?: HeadersInit,
  ): Promise<SuccessResponse<T>> {
    const reqHeaders = new Headers();
    reqHeaders.set("Content-Type", "application/json");
    if (headers) {
      Object.entries(headers).forEach(([key, value]) => {
        reqHeaders.set(key, value);
      });
    }
    const response = await fetch(`${this.baseURL}/api${url}`, {
      method,
      mode: import.meta.env.PROD ? "same-origin" : "cors",
      credentials: "include",
      body: JSON.stringify(body),
      headers: reqHeaders,
    });

    const data = await response.json();
    return data as SuccessResponse<T>;
  }

  async get<T>(
    url: string,
    headers?: HeadersInit,
  ): Promise<SuccessResponse<T>> {
    return this.request<T>(url, "GET", undefined, headers);
  }

  async post<T>(
    url: string,
    body?: any,
    headers?: HeadersInit,
  ): Promise<SuccessResponse<T>> {
    return this.request<T>(url, "POST", body, headers);
  }

  async put<T>(
    url: string,
    body?: any,
    headers?: HeadersInit,
  ): Promise<SuccessResponse<T>> {
    return this.request<T>(url, "PUT", body, headers);
  }

  async del<T>(
    url: string,
    headers?: HeadersInit,
  ): Promise<SuccessResponse<T>> {
    return this.request<T>(url, "DELETE", undefined, headers);
  }

  async patch<T>(
    url: string,
    body: any,
    headers?: HeadersInit,
  ): Promise<SuccessResponse<T>> {
    return this.request<T>(url, "PATCH", body, headers);
  }
}

export const API = Object.freeze(new Api());
