import { IBoardWithObjects } from "@/types/board";
import { API } from "@/utils/requests/API";
import { SuccessResponse } from "@/types/util";

export const getUserBoards = async (): Promise<
  SuccessResponse<IBoardWithObjects[]>
> => {
  return await API.get("/board-select/board");
};
