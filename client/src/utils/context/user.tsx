import { ILoginPayload, IRegisterPayload, IUser } from "@/types/user";
import React, { useContext, createContext, useReducer } from "react";
import { login as loginRequest } from "@/utils/requests/user/login";
import { logout as logoutRequest } from "@/utils/requests/user/logout";
import { register as registerRequest } from "@/utils/requests/user/register";

enum UserAction {
  LOGIN = "login",
  LOGOUT = "logout",
  REGISTER = "register",
  ERROR = "error",
}

interface IUserActions {
  login: (data: ILoginPayload, navigate: Function) => Promise<void>;
  logout: () => Promise<void>;
  register: (data: IRegisterPayload) => Promise<void>;
}

interface IUserContext {
  user: Partial<IUser>;
  actions: IUserActions;
  error: string;
}

interface IUserReducerAction {
  type: UserAction;
  payload: Partial<IUser>;
  error: string;
}

interface IUserReducer {
  (state: IUserContext, action: IUserReducerAction): IUserContext;
}

const initialState: IUserContext = {
  user: {},
  actions: {
    login: async () => {},
    logout: async () => {},
    register: async () => {},
  },
  error: "",
};

const UserContext = createContext<IUserContext>(initialState);

const userActions = (
  dispatch: React.Dispatch<IUserReducerAction>,
): IUserActions => ({
  login: async (data, navigate) => {
    const response = await loginRequest(data);
    console.log(response);
    if (!response.success) {
      return dispatch({
        type: UserAction.ERROR,
        payload: {},
        error: response.message,
      });
    }
    navigate("/boards/userboards");
    return dispatch({
      type: UserAction.LOGIN,
      payload: response.data!.user,
      error: "",
    });
  },
  logout: async () => {
    await logoutRequest();
    return dispatch({ type: UserAction.LOGOUT, payload: {}, error: "" });
  },
  register: async (data) => {
    const response = await registerRequest(data);
    if (!response.success) {
      return dispatch({
        type: UserAction.ERROR,
        payload: {},
        error: response.message,
      });
    }
    return dispatch({
      type: UserAction.REGISTER,
      payload: {},
      error: "",
    });
  },
});

const reducer: IUserReducer = (state, action) => {
  switch (action.type) {
    case UserAction.LOGIN:
      const loginObj = {
        ...state,
        user: action.payload,
        error: action.error,
      };
      localStorage.setItem("user", JSON.stringify(loginObj));
      return loginObj;
    case UserAction.REGISTER:
      const regObj = { ...state, ...action.payload, error: action.error };
      localStorage.setItem("user", JSON.stringify(regObj));
      return regObj;
    case UserAction.ERROR:
      const errObj = { ...state, error: action.error };
      localStorage.setItem("user", JSON.stringify(errObj));
      return errObj;
    case UserAction.LOGOUT:
      localStorage.removeItem("user");
      return { ...state, user: {}, error: "" };
    default:
      return state;
  }
};

const UserProvider = ({ children }: React.PropsWithChildren) => {
  const [state, dispatch] = useReducer<IUserReducer, IUserContext>(
    reducer,
    initialState,
    (arg: IUserContext) => {
      return JSON.parse(
        localStorage.getItem("user") ?? JSON.stringify(arg ?? initialState),
      );
    },
  );
  state.actions = userActions(dispatch);
  return (
    <UserContext.Provider value={{ ...state }}>{children}</UserContext.Provider>
  );
};

export const useUserContext = () => useContext(UserContext);

export default UserProvider;
