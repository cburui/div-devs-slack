import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { fileURLToPath, URL } from "url";
import generouted from "@generouted/react-router/plugin";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    // vite server configs, for details see [vite doc](https://vitejs.dev/config/#server-host)
    port: 4173,
    proxy: {
      "/api": "http://localhost:3000",
    },
  },
  preview: {
    port: 5555,
    proxy: {
      "/api": "http://localhost:3333",
    },
  },
  plugins: [react(), generouted()],
  resolve: {
    alias: [
      {
        find: "@/types/",
        replacement: fileURLToPath(new URL("../types/", import.meta.url)),
      },
      {
        find: "@/",
        replacement: fileURLToPath(new URL("./src/", import.meta.url)),
      },
    ],
  }
});
