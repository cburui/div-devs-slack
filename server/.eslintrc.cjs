const path = require("path");

module.exports = {
  root: true,
  env: { browser: false, es2020: true },
  extends: [
    "eslint:recommended",
    "plugin:import/recommended",
    "plugin:@typescript-eslint/recommended-type-checked",
    "plugin:@typescript-eslint/stylistic-type-checked",
  ],
  plugins: ["import", "import-resolver-alias"],
  settings: {
    "import/resolver": {
      typescript: {},
      alias: [["@/", path.resolve(__dirname, "./src/")]],
    },
  },
  ignorePatterns: ["dist", ".eslintrc.cjs"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    project: ["./tsconfig.json", "./tsconfig.node.json"],
    tsconfigRootDir: __dirname,
  },
};
