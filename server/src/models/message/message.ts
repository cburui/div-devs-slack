import { IMessage } from "@/types/message/message";
import { Schema, model } from "mongoose";

const messageSchema = new Schema<IMessage>({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  content: { type: String, required: true },
  createdAt: { type: Date, required: true, default: new Date() },
  channelId: { type: Schema.Types.ObjectId, required: true },
});
messageSchema.index({ name: "text", content: "text" });

const Message = model<IMessage>("Message", messageSchema);

export default Message;
