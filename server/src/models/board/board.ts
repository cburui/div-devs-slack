import { IBoard } from "@/types/board/index";
import { Schema, model } from "mongoose";

const boardSchema = new Schema<IBoard>({
  name: { type: String, required: true },
  admin: { type: Schema.Types.ObjectId, ref: "User", required: true },
  users: [{ type: Schema.Types.ObjectId, ref: "User", required: true }],
  channels: [{ type: Schema.Types.ObjectId, ref: "Channel", required: true }],
});

const Board = model<IBoard>("Board", boardSchema);

export default Board;
