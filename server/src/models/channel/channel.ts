import { IChannel } from "@/types/channel";
import { Schema, model } from "mongoose";

const channelSchema = new Schema<IChannel>({
  name: { type: String, required: true },
  users: [{ type: Schema.Types.ObjectId, ref: "User", required: true }],
  is_DM: { type: Boolean, required: true },
  createdAt: { type: Date, required: true, default: new Date() },
  pinnedMessages: [
    { type: Schema.Types.ObjectId, required: true, ref: "Message" },
  ],
});

const Channel = model<IChannel>("Channel", channelSchema);

export default Channel;
