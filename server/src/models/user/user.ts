import { CallbackWithoutResultAndOptionalError, Schema, model } from "mongoose";
import { passwordHash } from "@/utils/crypto";
import { IInvite, IUser } from "@/types/user";
import isEmail from "validator/lib/isEmail";

const userSchema = new Schema<IUser>(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: (email: string) =>
          isEmail(email, {
            host_whitelist: ["mail.mcgill.ca", "mcgill.ca"],
          }),
      },
    },
    username: { type: String, required: true },
    avatar: { type: String, required: false },
    password: { type: String, required: true },
    roles: [{ type: String, ref: "Role" }],
    createdAt: { type: Date, default: new Date() },
    invites: [
      new Schema<IInvite>({
        sender: { type: Schema.Types.ObjectId, ref: "User", required: true },
        board: { type: Schema.Types.ObjectId, ref: "Board", required: true },
      }),
    ],
  },
  {
    toJSON: {
      transform: (_doc, ret) => {
        delete ret.password;
        return ret;
      },
    },
  },
);

userSchema.pre("save", function (next: CallbackWithoutResultAndOptionalError) {
  // creating a SHA512 hash of the password with salt based on the SECRET environment variable
  this.password = passwordHash(this.password);
  next();
});

const User = model<IUser>("User", userSchema);

export default User;
