import { createHmac } from "crypto";

export const passwordHash = (password: string): string => {
  return createHmac("sha512", process.env.VITE_SECRET!)
    .update(password)
    .digest("hex");
};
