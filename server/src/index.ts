import dotenv from "dotenv";
import publicRouter from "./routes/public";
import privateRouter from "./routes/private";
import mongoose from "mongoose";
import express from "express";
import cors from "cors";
import session, { SessionOptions } from "express-session";
import MongoStore from "connect-mongo";
import path from "path";

dotenv.config();

export const isProduction = process.env.NODE_ENV === "production";
const clientPromise = mongoose
  .connect(process.env.VITE_MONGO_URI!)
  .then((mon) => mon.connection.getClient());
console.log("Connected to MongoDB at " + process.env.VITE_MONGO_URI);

export const hour = 1000 * 60 * 60;
export const month = hour * 24 * 30;
export const cookieExpiration =
  process.env.NODE_ENV === "production" ? month : hour;

const sessionOptions: SessionOptions = {
  secret: process.env.VITE_SECRET!,
  saveUninitialized: false, // don't create session until something stored
  resave: false, //don't save session if unmodified
  rolling: true,
  name: process.env.VITE_COOKIE_NAME!,
  cookie: {
    maxAge: cookieExpiration,
    httpOnly: true,
    secure: false,
    sameSite: "lax",
  },
  store: MongoStore.create({
    clientPromise,
    autoRemove: "interval",
    autoRemoveInterval: 10,
    crypto: {
      secret: process.env.VITE_SECRET!,
    },
  }),
};

const app = express();

if (isProduction) {
  app.set("trust proxy", 1); // trust first proxy
  //sessionOptions.cookie!.secure = true; // serve secure cookies
}

const healthRouter = express.Router();
healthRouter.route("/healthcheck").get((_req, res) => {
  res.status(200).send("Hello there!");
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(session(sessionOptions));
app.use(
  cors({
    credentials: true,
    origin: process.env.VITE_CLIENT_URL,
    exposedHeaders: ["set-cookie"],
  }),
);

if (isProduction) {
  app.use('/', (req, _res, next) => {
    req.url = req.url.replace(/(?<!:)\/+/gm, '/');
    next();
  }, healthRouter, publicRouter, privateRouter);
} else {
  app.use('/api', healthRouter, publicRouter, privateRouter);
}

// For development mode, the server is started by Vite, so we only need to do this for production
if (isProduction) {
  const staticServer = express();
  staticServer.use(express.static('public'));
  staticServer.get('*', (_req, res) => res.sendFile(path.resolve('public', 'index.html')));
  staticServer.listen(3000, () => {
    console.log(`Frontend at http://localhost:3000`);
  });
  app.listen(5000, () => {
    console.log(`Backend at http://localhost:5000`);
  });
}

export const viteNodeApp = app;
