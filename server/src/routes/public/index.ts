import express from "express";
import { login } from "@/routes/public/user/login";
import { register } from "@/routes/public/user/register";

const router = express.Router();
router.post("/login", login);
router.post("/register", register);

export default router;
