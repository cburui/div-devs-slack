import User from "@/models/user/user";
import { passwordHash } from "@/utils/crypto";
import { Request, Response } from "express";
import { ILoginPayload, ILoginResponse } from "@/types/user";
import { IRequest } from "@/types/requests/session";
import { SuccessResponse } from "@/types/util";

export const login = async (req: Request, res: Response) => {
  let payload: SuccessResponse<ILoginResponse>;
  try {
    const { email, password } = req.body as ILoginPayload;
    const user = await User.findOne({
      email,
      password: passwordHash(password),
    });
    if (!user) {
      res
        .status(401)
        .json({
          message: "These email and password are invalid.",
          success: false,
        });
      return;
    }
    const ireq = req as IRequest;
    ireq.session.user = user;
    payload = {
      message: "Logged in successfully",
      data: { user },
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = {
      message: "These email and password are invalid.",
      success: false,
    };
    res.status(401).json(payload);
  }
};
