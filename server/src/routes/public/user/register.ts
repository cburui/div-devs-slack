import { Request, Response } from "express";
import User from "@/models/user/user";
import { IRegisterPayload } from "@/types/user";
import { SuccessResponse } from "@/types/util";

export const register = async (req: Request, res: Response) => {
  let payload: SuccessResponse<void>;
  try {
    const { email, password, username } = req.body as IRegisterPayload;
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      payload = { message: "User already exists", success: false };
      res.status(400).json(payload);
      return;
    }
    await User.create({ email, password, username, createdAt: new Date() });
    payload = { message: "User created successfully", success: true };
    res.status(201).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};
