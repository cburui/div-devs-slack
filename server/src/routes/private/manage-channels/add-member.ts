import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import User from "@/models/user/user";
import { SuccessResponse } from "@/types/util";
import { IChannel } from "@/types/channel";

const addMemberChannel = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IChannel>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.channelId)) {
      payload = { message: "Invalid channel id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channel = await Channel.findById(req.params.channelId);
    if (channel === null) {
      payload = { message: "Channel does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.channels.includes(channel._id)) {
      payload = { message: "Could not be added", success: false };
      res.status(400).json(payload);
      return;
    }
    if (channel.is_DM) {
      payload = { message: "Could not be added", success: false };
      res.status(400).json(payload);
      return;
    }
    const userToAdd = await User.findOne({ email: req.body.email });
    if (userToAdd === null) {
      payload = { message: "The user does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userToAdd._id)) {
      payload = {
        message: "Can only add members of the board",
        success: false,
      };
      res.status(400).json(payload);
      return;
    }
    if (channel.users.includes(userToAdd._id)) {
      payload = {
        message: "User is already a member of the channel",
        success: false,
      };
      res.status(400).json(payload);
      return;
    }
    await Channel.updateOne(
      { _id: req.params.channelId },
      { $push: { users: userToAdd._id } },
      { new: true },
    );
    payload = {
      message: "User successfully added to the channel",
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: "Could not add member", success: false };
    res.status(400).json(payload);
  }
};

export default addMemberChannel;
