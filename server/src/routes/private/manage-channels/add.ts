import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import { IChannel } from "@/types/channel";
import { SuccessResponse } from "@/types/util";

const addChannel = async (req: Request, res: Response) => {
  let payload: SuccessResponse<IChannel>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channel = await Channel.create({
      name: req.body.name,
      users: [],
      is_DM: false,
    });
    await Board.updateOne(
      { _id: req.params.boardId },
      { $push: { channels: channel } },
    );
    payload = {
      message: "Channel created successfully!",
      data: channel.toJSON(),
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default addChannel;
