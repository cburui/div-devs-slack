import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import { SuccessResponse } from "@/types/util";
import { IChannel } from "@/types/channel";

const renameChannel = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IChannel>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.channelId)) {
      payload = { message: "Invalid channel id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channel = await Channel.findById(req.params.channelId);
    if (channel === null) {
      payload = { message: "Channel does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.channels.includes(channel._id)) {
      payload = { message: "Could not rename channel", success: false };
      res.status(400).json(payload);
    }
    if (channel.is_DM) {
      payload = { message: "Could not rename channel", success: false };
      res.status(400).json(payload);
      return;
    }
    const update = await Channel.findOneAndUpdate(
      { _id: req.params.channelId },
      { $set: { name: req.body.name } },
      { new: true },
    );
    payload = {
      message: "Channel successfully renamed!",
      data: update,
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default renameChannel;
