import express from "express";
import addChannel from "./add";
import removeChannel from "./remove";
import renameChannel from "./rename";
import addMemberChannel from "./add-member";
import removeMemberChannel from "./remove-member";
import refreshChannelsBoard from "./refresh";

const router = express.Router();
router
  .route("/board-select/board/:boardId/channel")
  .get(refreshChannelsBoard)
  .post(addChannel);
router
  .route("/board-select/board/:boardId/channel/:channelId")
  .put(renameChannel)
  .delete(removeChannel);
router
  .route("/board-select/board/:boardId/channel/:channelId/member")
  .put(addMemberChannel);
router
  .route("/board-select/board/:boardId/channel/:channelId/member/:userId")
  .delete(removeMemberChannel);

export default router;
