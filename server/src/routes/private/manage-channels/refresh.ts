import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import { IChannel } from "@/types/channel";
import { SuccessResponse } from "@/types/util";

const refreshChannelsBoard = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IChannel[]>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findOne({ _id: req.params.boardId }).populate<{
      channels: IChannel[];
    }>({
      path: "channels",
      populate: [
        { path: "users", model: "User" },
        { path: "pinnedMessages", model: "Message" },
      ],
    });
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channels = board.channels;
    payload = { message: "Refreshed channels!", data: channels, success: true };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default refreshChannelsBoard;
