import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import User from "@/models/user/user";
import { IBoard } from "@/types/board";
import { SuccessResponse } from "@/types/util";
import Channel from "@/models/channel/channel";

const addMemberBoard = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IBoard>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const userToAdd = await User.findOne({ email: req.body.email });
    if (userToAdd === null) {
      payload = { message: "The user does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (board.users.includes(userToAdd._id)) {
      payload = { message: "User is already a member", success: false };
      res.status(400).json(payload);
      return;
    }
    const update = await Board.findOneAndUpdate(
      { _id: req.params.boardId },
      { $push: { users: userToAdd } },
      { new: true },
    );
    //const generalChannelId = board.channels.find(channel => channel.name === "general")?._id;
    await Channel.updateOne(
      { name: "general", boardId: req.params.boardId },
      { $push: { users: userToAdd } },
    );
    payload = {
      message: "Successfully added new member!",
      data: update,
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default addMemberBoard;
