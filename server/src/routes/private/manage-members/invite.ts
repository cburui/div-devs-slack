import { Request, Response } from "express";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import User from "@/models/user/user";
import { SuccessResponse } from "@/types/util";
import mongoose from "mongoose";

const inviteMemberBoard = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<void>;
  try {
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const userToAdd = await User.findOne({ email: req.body.email });
    if (userToAdd === null) {
      payload = { message: "User does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (board.users.includes(userToAdd._id)) {
      payload = { message: "User is already a member", success: false };
      res.status(400).json(payload);
      return;
    }
    await User.updateOne(
      { email: req.body.email },
      { $push: { invites: { board: board._id, sender: userData._id } } },
    );
    payload = { message: "Successfully invited new member!", success: true };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default inviteMemberBoard;
