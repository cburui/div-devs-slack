import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import User from "@/models/user/user";
import Channel from "@/models/channel/channel";
import { SuccessResponse } from "@/types/util";

const removeMemberBoard = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<void>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.userId)) {
      payload = { message: "Invalid user id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const userToRemove = await User.findById(req.params.userId);
    if (userToRemove === null) {
      payload = { message: "User does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userToRemove._id)) {
      payload = { message: "User is not a member", success: false };
      res.status(400).json(payload);
      return;
    }
    if (userToRemove._id.equals(userData._id)) {
      payload = { message: "Cannot remove yourself", success: false };
      res.status(400).json(payload);
      return;
    }
    await Channel.updateMany(
      { _id: { $in: board.channels } },
      { $pull: { users: userToRemove._id } },
    );
    await Board.updateOne(
      { _id: req.params.boardId },
      { $pull: { users: userToRemove._id } },
    );
    payload = { message: "Successfully removed member!", success: true };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default removeMemberBoard;
