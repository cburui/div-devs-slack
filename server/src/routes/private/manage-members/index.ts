import express from "express";
import addMemberBoard from "./add";
import refreshMembersBoard from "./refresh";
import removeMemberBoard from "./remove";

const router = express.Router();
router
  .route("/board-select/board/:boardId/member")
  .get(refreshMembersBoard)
  .put(addMemberBoard);
router
  .route("/board-select/board/:boardId/member/:userId")
  .delete(removeMemberBoard);

export default router;
