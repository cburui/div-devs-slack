import { logout } from "./logout";
import { getProfile } from "./profile";
import express from "express";

const router = express.Router();
router.delete("/logout", logout);
router.get("/profile", getProfile);

export default router;
