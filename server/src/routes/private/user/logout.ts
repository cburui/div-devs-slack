import { IRequest } from "@/types/requests/session";
import { Request, Response } from "express";

export const logout = async (req: Request, res: Response) => {
  const ireq = req as IRequest;
  try {
    ireq.session.destroy((err) => {
      if (err) {
        res.status(500).json({ message: "Something went wrong" });
        return;
      } else {
        res
          .clearCookie(process.env.VITE_COOKIE_NAME!)
          .status(200)
          .json({ message: "Logged out successfully" });
      }
    });
  } catch (error) {
    res
      .clearCookie(process.env.VITE_COOKIE_NAME!)
      .status(500)
      .json({ message: "Something went wrong" });
  }
};
