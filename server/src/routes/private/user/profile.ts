import User from "@/models/user/user";
import { IRequest } from "@/types/requests/session";
import { Request, Response } from "express";

export const getProfile = async (req: Request, res: Response) => {
  try {
    const ireq = req as IRequest;
    const user = await User.findOne({ email: ireq.session.user.email });
    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: "User not found" });
  }
};
