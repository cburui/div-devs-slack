import { Request, Response } from "express";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import User from "@/models/user/user";
import { SuccessResponse } from "@/types/util";
import { IBoard } from "@/types/board";
import mongoose from "mongoose";

const acceptInvite = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IBoard>;
  try {
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    const invite = (userData.invites ?? []).find(
      (invite) => invite.board.toString() === req.params.boardId,
    );
    if (!invite) {
      payload = { message: "Invite does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.admin.equals(invite.sender)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    if (board.users.includes(userData._id)) {
      payload = { message: "You are already a member", success: false };
      res.status(400).json(payload);
      return;
    }
    const update = await Board.findOneAndUpdate(
      { _id: req.params.boardId },
      { $push: { users: userData._id } },
      { new: true },
    );
    await User.updateOne(
      { _id: userData._id },
      { $pull: { invites: { board: board._id, sender: board.admin } } },
      { new: true },
    );
    payload = {
      message: "Successfully accepted the invite!",
      data: update,
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default acceptInvite;
