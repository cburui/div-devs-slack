import express from "express";
import refreshChannel from "./refresh";
import refreshChannelMessages from "./refresh-messages";
import createMessageChannel from "./create-message";
import removeMessageChannel from "./remove-message";
import pinMessage from "./pin-message";
import unpinMessage from "./unpin-message";
import searchMessage from "./search-message";

const router = express.Router();
router
  .route("/board-select/board/:boardId/channel/:channelId")
  .get(refreshChannel);
router
  .route("/board-select/board/:boardId/channel/:channelId/message")
  .get(refreshChannelMessages)
  .post(createMessageChannel);
router
  .route("/board-select/board/:boardId/channel/:channelId/message/search")
  .get(searchMessage);
router
  .route("/board-select/board/:boardId/channel/:channelId/message/:messageId")
  .delete(removeMessageChannel);
router
  .route(
    "/board-select/board/:boardId/channel/:channelId/message/:messageId/pin",
  )
  .put(pinMessage)
  .delete(unpinMessage);

export default router;
