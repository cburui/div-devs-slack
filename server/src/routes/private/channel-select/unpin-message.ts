import Channel from "@/models/channel/channel";
import { IRequest } from "@/types/requests/session";
import { SuccessResponse } from "@/types/util";
import { Request, Response } from "express";
import mongoose from "mongoose";

const unpinMessage = async (req: Request, res: Response) => {
  const ireq = req as IRequest;
  const userData = ireq.session.user;
  let payload: SuccessResponse<void>;
  if (!mongoose.Types.ObjectId.isValid(req.params.channelId)) {
    payload = { message: "Invalid channel id", success: false };
    res.status(400).json(payload);
    return;
  }
  if (!mongoose.Types.ObjectId.isValid(req.params.messageId)) {
    payload = { message: "Invalid message id", success: false };
    res.status(400).json(payload);
    return;
  }
  const channel = await Channel.findById(req.params.channelId);
  if (channel === null) {
    payload = { message: "Channel does not exist", success: false };
    res.status(400).json(payload);
    return;
  }
  if (!channel.users.includes(userData._id)) {
    payload = { message: "Permission denied", success: false };
    res.status(401).json(payload);
    return;
  }
  await Channel.updateOne(
    { _id: req.params.channelId },
    { $pull: { pinnedMessages: req.params.messageId } },
  );
  payload = { message: "Message unpinned successfully!", success: true };
  res.status(200).json(payload);
};

export default unpinMessage;
