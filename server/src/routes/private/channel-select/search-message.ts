import Channel from "@/models/channel/channel";
import Message from "@/models/message/message";
import { IMessageWithObjects } from "@/types/message/message";
import { IRequest } from "@/types/requests/session";
import { IUser } from "@/types/user";
import { SuccessResponse } from "@/types/util";
import { Request, Response } from "express";
import mongoose from "mongoose";

const messagesPerPage = 20;

const searchMessage = async (
  req: Request,
  res: Response
) => {
  const ireq = req as IRequest;
  const userData = ireq.session.user;
  const { channelId } = req.params;
  const searchText = req.query.searchText as string;
  const page = req.query.page as string;
  let payload: SuccessResponse<IMessageWithObjects[]>;
  if (!mongoose.Types.ObjectId.isValid(channelId)) {
    payload = { message: "Invalid channel id", success: false };
    res.status(400).json(payload);
    return;
  }
  const channel = await Channel.findById(channelId);
  if (channel === null) {
    res.status(400).json({ message: "Channel does not exist" });
    return;
  }
  if (!channel.users.includes(userData._id)) {
    res.status(401).json({ message: "Permission denied" });
    return;
  }
  const messages = await Message.find({
    $text: { $search: searchText },
    channelId: req.params.channelId,
  })
    .sort({ createdAt: -1 })
    .skip((parseInt(page) - 1) * messagesPerPage)
    .limit(messagesPerPage)
    .populate<{ user: IUser }>("user")
    .exec();
  payload = {
    message: "Messages successfully retrieved!",
    data: messages,
    success: true,
  };
  res.status(200).json(payload);
};

export default searchMessage;
