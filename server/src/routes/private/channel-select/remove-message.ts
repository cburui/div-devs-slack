import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import Message from "@/models/message/message";
import { SuccessResponse } from "@/types/util";
import { IMessage } from "@/types/message/message";

const removeMessageChannel = async (
  req: Request,
  res: Response,
) => {
  let payload: SuccessResponse<IMessage>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.channelId)) {
      payload = { message: "Invalid channel id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.messageId)) {
      payload = { message: "Invalid message id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channel = await Channel.findById(req.params.channelId);
    if (channel === null) {
      payload = { message: "Channel does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (channel.is_DM) {
      payload = { message: "Message could not be created", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!channel.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const message = await Message.findById(req.params.messageId);
    if (message === null) {
      payload = { message: "Message does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!message.channelId.equals(channel._id)) {
      payload = {
        message: "Message does not belong to this channel",
        success: false,
      };
      res.status(400).json(payload);
      return;
    }
    if (
      !message.user.equals(userData._id) &&
      !board.admin.equals(userData._id)
    ) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    await Channel.updateOne(
      { _id: req.params.channelId },
      { $pull: { pinnedMessages: req.params.messageId } },
    );
    await Message.deleteOne({ _id: req.params.messageId });
    payload = { message: "Message removed successfully!", success: true };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default removeMessageChannel;
