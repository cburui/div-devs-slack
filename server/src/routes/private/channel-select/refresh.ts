import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import { SuccessResponse } from "@/types/util";
import { IMessageWithObjects } from "@/types/message/message";
import { IUser } from "@/types/user";
import { IChannelWithObject } from "@/types/channel";

const refreshChannelMessages = async (req: Request, res: Response) => {
  let payload: SuccessResponse<IChannelWithObject>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.channelId)) {
      payload = { message: "Invalid channel id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channel = await Channel.findById(req.params.channelId)
      .populate<{ users: IUser[] }>("users")
      .populate<{ pinnedMessages: IMessageWithObjects[] }>("pinnedMessages")
      .exec();
    console.log(channel);

    payload = {
      message: "Messages successfully refreshed!",
      data: channel,
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default refreshChannelMessages;
