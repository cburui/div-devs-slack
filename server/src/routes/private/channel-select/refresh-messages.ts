import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import Message from "@/models/message/message";
import { SuccessResponse } from "@/types/util";
import { IMessageWithObjects } from "@/types/message/message";
import { IUser } from "@/types/user";

const messagesPerPage = 20;

const refreshChannelMessages = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IMessageWithObjects[]>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.channelId)) {
      payload = { message: "Invalid channel id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const channel = await Channel.findById(req.params.channelId);
    if (channel === null) {
      payload = { message: "Channel does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!channel.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    console.log(req.query);
    const page = parseInt((req.query.page ?? "1") as string);
    const messages = await Message.find({ channelId: channel._id })
      .sort({ createdAt: -1 })
      .skip((page - 1) * messagesPerPage)
      .limit(messagesPerPage)
      .populate<{ user: IUser }>("user")
      .exec();
    payload = {
      message: "Messages successfully refreshed!",
      data: messages ?? [],
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default refreshChannelMessages;
