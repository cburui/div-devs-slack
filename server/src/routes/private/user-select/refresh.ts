import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import Channel from "@/models/channel/channel";
import Message from "@/models/message/message";
import User from "@/models/user/user";
import { SuccessResponse } from "@/types/util";
import { IMessageWithObjects } from "@/types/message/message";
import { IUser } from "@/types/user";

const messagesPerPage = 20;

const refreshUserMessages = async (
  req: Request,
  res: Response
) => {
  let payload: SuccessResponse<IMessageWithObjects[]>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!mongoose.Types.ObjectId.isValid(req.params.userId)) {
      payload = { message: "Invalid user id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId);
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userData._id)) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    const userToMessage = await User.findById(req.params.userId);
    if (userToMessage === null) {
      payload = { message: "User does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (userToMessage._id.equals(userData._id)) {
      payload = { message: "Cannot message yourself", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.includes(userToMessage._id)) {
      payload = {
        message: "Can only message members of the board",
        success: false,
      };
      res.status(400).json(payload);
      return;
    }
    const dm = await Channel.findOne({
      is_DM: true,
      users: { $in: [userData._id, userToMessage._id] },
    });
    if (dm === null) {
      payload = {
        message: "Messages refreshed successfully!",
        data: [],
        success: true,
      };
      res.status(200).json(payload);
      return;
    }
    const messages = await Message.find({ channelId: dm._id })
      .sort({ createdAt: -1 })
      .skip(messagesPerPage * parseInt(req.params.page))
      .limit(messagesPerPage)
      .populate<{ user: IUser }>("user")
      .exec();
    payload = {
      message: "Messages refreshed successfully!",
      data: messages,
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default refreshUserMessages;
