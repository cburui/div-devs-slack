import express from "express";
import refreshUserMessages from "./refresh";
import createMessageUser from "./create-message";
import removeMessageUser from "./remove-message";

const router = express.Router();
router
  .route("/board-select/board/:boardId/member/:userId/message")
  .get(refreshUserMessages)
  .post(createMessageUser);
router
  .route("/board-select/board/:boardId/member/:userId/message/:messageId")
  .delete(removeMessageUser);

export default router;
