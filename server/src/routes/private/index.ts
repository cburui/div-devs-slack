import express, { Request, Response, NextFunction } from "express";
import userRoutes from "./user";
import boardSelectRoutes from "./board-select";
import manageMembersRoutes from "./manage-members";
import manageChannelsRoutes from "./manage-channels";
import channelSelectRoutes from "./channel-select";
import userSelectRoutes from "./user-select";
import { IRequest } from "@/types/requests/session";

const router = express.Router();
router.use((req: Request, res: Response, next: NextFunction) => {
  const ireq = req as IRequest;
  if (!(ireq.url.includes("/login") || ireq.url.includes("/register") || ireq.url.includes("/healthcheck")) && !ireq.session.user) {
    res.status(401).json({ message: "You are not logged in" });
  } else {
    next();
  }
});
router.use("/user", userRoutes);
router.use("/board-select", boardSelectRoutes);
router.use(manageMembersRoutes);
router.use(manageChannelsRoutes);
router.use(channelSelectRoutes);
router.use(userSelectRoutes);

export default router;
