import express from "express";
import refresh from "./refresh";
import create from "./create";
import remove from "./remove";
import select from "./select";

const router = express.Router();
router.route("/board").get(refresh).post(create);
router.route("/board/:boardId").get(select).delete(remove);

export default router;
