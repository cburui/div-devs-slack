import { Request, Response } from "express";
import mongoose from "mongoose";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import { SuccessResponse } from "@/types/util";
import { IBoardWithObjects } from "@/types/board";
import { IUser } from "@/types/user";
import { IChannelWithObject } from "@/types/channel";

const select = async (req: Request, res: Response) => {
  let payload: SuccessResponse<IBoardWithObjects>;
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.boardId)) {
      payload = { message: "Invalid board id", success: false };
      res.status(400).json(payload);
      return;
    }
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const board = await Board.findById(req.params.boardId)
      .populate<{ users: IUser[] }>("users")
      .populate<{ channels: IChannelWithObject[] }>({
        path: "channels",
        populate: [
          { path: "users", model: "User" },
          { path: "pinnedMessages", model: "Message" },
        ],
      })
      .populate<{ admin: IUser }>("admin");
    if (board === null) {
      payload = { message: "Board does not exist", success: false };
      res.status(400).json(payload);
      return;
    }
    if (!board.users.find((user) => user._id.equals(userData._id))) {
      payload = { message: "Permission denied", success: false };
      res.status(401).json(payload);
      return;
    }
    payload = {
      message: "Board selected successfully!",
      data: board,
      success: true,
    };
    console.log(payload.data?.channels[0].pinnedMessages);

    res.status(200).json(payload);
  } catch (error: any) {
    payload = { message: error.message, success: false };
    res.status(500).json(payload);
  }
};

export default select;
