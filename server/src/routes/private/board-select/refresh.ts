import { Request, Response } from "express";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import { SuccessResponse } from "@/types/util";
import { IBoardWithObjects } from "@/types/board";
import { IUser } from "@/types/user";
import { IChannelWithObject } from "@/types/channel";

const refresh = async (req: Request, res: Response) => {
  try {
    const ireq = req as IRequest;
    const userData = ireq.session.user;
    const boards = await Board.find({ users: userData._id })
      .populate<{ admin: IUser }>("admin")
      .populate<{ users: IUser[] }>("users")
      .populate<{ channels: IChannelWithObject[] }>({
        path: "channels",
        populate: [
          { path: "users", model: "User" },
          { path: "pinnedMessages", model: "Message" },
        ],
      });
    const payload: SuccessResponse<IBoardWithObjects[]> = {
      message: "Refreshed discussion boards!",
      data: boards,
      success: true,
    };
    console.log(payload);
    res.status(200).json(payload);
  } catch (error: any) {
    const payload: SuccessResponse<IBoardWithObjects[]> = {
      message: "Failed to refresh discussion boards.",
      success: false,
    };
    res.status(400).json(payload);
  }
};

export default refresh;
