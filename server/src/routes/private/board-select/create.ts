import { Request, Response } from "express";
import Board from "@/models/board/board";
import { IRequest } from "@/types/requests/session";
import { SuccessResponse } from "@/types/util";
import { IBoard } from "@/types/board";
import Channel from "@/models/channel/channel";

const create = async (req: Request, res: Response) => {
  try {
    const ireq = req as IRequest;
    const user = ireq.session.user;
    const channel = await Channel.create({
      name: "general",
      users: [user],
      is_DM: false,
      pinnedMessages: [],
    });
    const board = await Board.create({
      name: req.body.name,
      admin: user._id,
      users: [user._id],
      channels: [channel._id],
    });
    const payload: SuccessResponse<IBoard> = {
      message: "Board successfully created!",
      data: board,
      success: true,
    };
    res.status(200).json(payload);
  } catch (error: any) {
    const payload: SuccessResponse<IBoard> = {
      message: "Failed to create board.",
      success: false,
    };
    res.status(400).json(payload);
  }
};

export default create;
